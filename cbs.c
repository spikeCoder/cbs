/* Catch Bird Singing
 *
 * $Id: cbs.c,v 1.28 2005/11/22 07:17:38 bdwright Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
/* #include <asm/io.h> */
#include <sys/io.h>  /* needed for glibc ioperm */
#include <unistd.h>
#include <string.h>
#include <values.h>
#include <linux/soundcard.h>
#if defined (USE_CURSES)
# include <ncurses.h>
#endif

#define DEFINE_GLOBALS
#include "cbs.h"

int 
main (int argc, char *argv[], char *envp[])
{
    int audioDevice; 
    /* int IOPort = 0x220; */ 
    char *filenamePrefix;
    float tmpTime;

    /* Switch off the automatic gain control on soundcard. JCR */
    /* Do something with SOUND_MIXER_AGC ioctl? BDW */
    /* Use ioctl(fd, SOUND_MIXER_AGC, &mode) to turn AGC off (0) or on (1) */
/*	 if (getuid() == ROOT_UID) {
	   ioperm((unsigned long)IOPort, (unsigned long)6, 666);
	   outb(AGCREG, IOPort + MIXER_ADDR);
	   outb(0x01, IOPort + MIXER_DATA); 
	 }
*/

    fprintf (stderr, "Free space at start %7d\n", diskSpace ("."));

    

    parseCommandLine (argc, argv);
    initializeValues ();
    /* sanity check! */
    tmpTime = MAX (preTriggerTime, postTriggerTime);
    if (tmpTime > gapTime) {
	gapTime = tmpTime;
    }

#if defined (USE_CURSES)
    initializeScreen ();
#endif

    initFiles (&logFile, &filenamePrefix, animalNumber);

    /* initialize the soundcard. NOTE that this sets the global chunkSize */
    audioDevice = initHardware ();

    /* got chunkSize, determine numChunks */
    numChunks = initNumChunks();

    initChunks (numChunks, chunkSize);

    preMainLoopLogs ();
    /* MAIN LOOP */
    getSong (audioDevice, filenamePrefix);

    /* clean up */
    free (mainBuffer);
    free (bufferL);
    free (bufferR);
    close (audioDevice);

#if defined (USE_CURSES)
    clear ();
    refresh ();
    nocbreak ();
    endwin ();                  /* for curses */
#endif
    fprintf (stderr, "Free space at end   %7d\n", diskSpace ("."));
    exit (0);
}

void 
usage (void)
{
/*  const char *usage_string = "cbs -[options] filebasename"; */

    fprintf (stderr, "Usage: cbs -[%s]\n", ARGLIST);
    fprintf (stderr, "Options: f num -- usb audio device number (default 0) ***usbcbs***\n");
    fprintf (stderr, "\t v num -- recording volume as a percentage 0-100. Defaults to 100\%  ***usbcbs***\n");
    fprintf (stderr, "\t a str -- animal number (prefix for auto file name)\n");
    fprintf (stderr, "\t b num -- binsize (ms) for analysis. Default: %f\n", binMSTime);
    fprintf (stderr, "\t c num -- Trigger criteria: 0 = level (mono) or L,R combined level (stereo);\n");
    fprintf (stderr, "\t                            1 = L or R (stereo);\n");
    fprintf (stderr, "\t                            2 = L and R (stereo);\n");
    fprintf (stderr, "\t                            3 = L (stereo);\n");
    fprintf (stderr, "\t                            4 = R (stereo);\n");
    fprintf (stderr, "\t                            5 = L not R (stereo);\n");
    fprintf (stderr, "\t                            6 = R not L (stereo);\n");
    fprintf (stderr, "\t                            7 = (L not R) or (R not L) (stereo);\n");
    fprintf (stderr, "\t                            Default: %d (mono), %d (stereo)\n", trigCriteria, L_OR_R);
    fprintf (stderr, "\t u num -- Quiet criteria:   0 = level (mono) or L,R combined level (stereo);\n");
    fprintf (stderr, "\t                            1 = L and R (stereo);\n");
    fprintf (stderr, "\t                            2 = L (stereo);\n");
    fprintf (stderr, "\t                            3 = R (stereo);\n");
    fprintf (stderr, "\t                            4 = L or R (stereo);\n");
    fprintf (stderr, "\t                            Default: %d (mono), %d (stereo)\n", quietCriteria, QL_AND_R);
    fprintf (stderr, "\t d     -- print verbose debugging output\n");
    fprintf (stderr, "\t g num -- minimum gap length for new song (sec). Default: %f\n", gapTime);
    fprintf (stderr, "\t h     -- this usage message\n");
    fprintf (stderr, "\t m num -- minimum song duration (sec). Default: %f\n", minDurationTime);
    fprintf (stderr, "\t n num -- number of recording channels (1 = mono, 2 = stereo). Default: %d\n", 1);
    fprintf (stderr, "\t p num -- length (sec) of pretrigger segment to keep. Default: %f\n", preTriggerTime);
    fprintf (stderr, "\t q num -- length (sec) of posttrigger segment to keep. Default: %f\n", postTriggerTime);
    fprintf (stderr, "\t s num -- sample frequency (Hz). Default: %ld\n", WHead.sample_fq);
    fprintf (stderr, "\t t num -- threshold value for detecting singing. Default: %.2f\n", songThresh);
    fprintf (stderr, "\t x file -- mixer device [%s]\n", mixer_device);
    exit (-1);
}


/* shamelessly stolen from example in getopt(3) man page */
int 
parseCommandLine (int ac, char *av[])
{
    int tempInt;
    int ch;
    float tempFloat;
    extern int optind;
    extern char *optarg;
    BOOLEAN setTrigCriteria = FALSE;
    BOOLEAN setQuietCriteria = FALSE;
    char *env;

    env = getenv("CBS_AUDIO_DEVICE");
    if(env) audio_device = env;

    env = getenv("CBS_MIXER_DEVICE");
    if(env) mixer_device = env;

    while ((ch = getopt (ac, av, "a:b:c:df:g:hm:n:p:q:s:t:u:x:")) != EOF)
        switch (ch) {
            case 'a':
                strcpy (animalNumber, optarg);
                break;
            case 'b':
                tempFloat = atof (optarg);
                if (tempFloat < 0.0) {
                    fprintf (stderr, "cbs: illegal value for binMSTime. Using default: %f.\n",
                             binMSTime);
                } else {
                    binMSTime = tempFloat;
                }
                break;
            case 'c':
                tempInt = atoi (optarg);
		setTrigCriteria = TRUE;
                switch (tempInt) {
                    case LEVEL_CRIT:
                        trigCriteria = LEVEL_CRIT;
                        break;
                    case L_OR_R:
                        trigCriteria = L_OR_R;
                        break;
                    case L_AND_R:
                        trigCriteria = L_AND_R;
                        break;
                    case L:
                        trigCriteria = L;
                        break;
                    case R:
                        trigCriteria = R;
                        break;
                    case L_NOT_R:
                        trigCriteria = L_NOT_R;
                        break;
                    case R_NOT_L:
                        trigCriteria = R_NOT_L;
                        break;
                    case L_NOT_R_OR_R_NOT_L:
                        trigCriteria = L_NOT_R_OR_R_NOT_L;
                        break;
                    default:
                        fprintf (stderr, "cbs: illegal value for trigger criteria. Using default: %d.\n",
                                 trigCriteria); 
			setTrigCriteria = FALSE;
                        break;
		    }
                break;
            case 'd':           /* verbose debugging */
                debugflag = TRUE;
                break;
	    case 'f':
		audio_device = optarg;
		break;
            case 'g':           /* minimum gap length to start new song  */
                tempFloat = atof (optarg);
                if (tempFloat < 0.0 || tempFloat > 10000.0) {
                    fprintf (stderr, "cbs: illegal value %f msec for minimum gap.\n",
                             tempFloat);
                    fprintf (stderr, "cbs: Using default of %f sec.\n", gapTime);
                } else {
                    gapTime = tempFloat;
                }
                break;
            case 'm':
                tempFloat = atof (optarg);
                if (tempFloat <= 0) {
                    fprintf (stderr, "cbs: illegal value %f sec for minDuration sampling.\n", tempFloat);
                    fprintf (stderr, "cbs: Using default of %f sec.\n", minDurationTime);
                } else {
                    minDurationTime = tempFloat;
                }
                break;
            case 'n':
                tempInt = atoi (optarg);
                switch (tempInt) {
		    case WAV_MONO: 
                        WHead.channels = tempInt;
			mono_mode = 1;
                        break;
		    case WAV_STEREO:
                        WHead.channels = tempInt;
			mono_mode = 0;
                        break;
                    default:
                        fprintf (stderr, "cbs: illegal value for number of channels. Using default: %d.\n",
                                 WHead.channels);
                        break;
                }
                break;
            case 'p':           /* how much pretrigger data to store (sec) */
                tempFloat = atof (optarg);
                if (tempFloat < 0 || tempFloat > 20) {
                    fprintf (stderr, "cbs: illegal value %f sec for preTrigger sampling.\n", tempFloat);
                    fprintf (stderr, "cbs: Using default of %f sec.\n", preTriggerTime);
                } else {
                    preTriggerTime = tempFloat;
                }
                break;
            case 'q':           /* how much postTrigger data to store (sec) */
                tempFloat = atof (optarg);
                if (tempFloat < 0 || tempFloat > 20) {
                    fprintf (stderr, "cbs: illegal value %f sec for postTrigger sampling.\n", tempFloat);
                    fprintf (stderr, "cbs: Using default of %f sec.\n", postTriggerTime);
                } else {
                    postTriggerTime = tempFloat;
                }
                break;
            case 's':           /* sampling frequency */
                tempInt = atoi (optarg);
                if (tempInt <= 500) {
                    fprintf (stderr, "cbs: illegal value for speed. Using default: %ld Hz.\n",
                             WHead.sample_fq);
                } else {
		  WHead.sample_fq = tempInt;		  
                }
                break;
            case 't':
                tempFloat = atof (optarg);
                if (tempFloat <= 0.0) {
                    fprintf (stderr, "cbs: Threshold must be positive. Using default: %f\n",
                             songThresh);
                } else {
                    songThresh = tempFloat;
                    songThreshL = songThresh;
                    songThreshR = songThresh;

                    if (debugflag) {
                        fprintf (stderr, "Threshold set to %f\n", songThresh);
                    }
                }
                break;
            case 'u':
                tempInt = atoi (optarg);
		setQuietCriteria = TRUE;
                switch (tempInt) {
                    case QLEVEL_CRIT:
                        quietCriteria = QLEVEL_CRIT;
                        break;
                    case QL_AND_R:
                        quietCriteria = QL_AND_R;
                        break;
                    case QL:
                        quietCriteria = QL;
                        break;
                    case QR:
                        quietCriteria = QR;
                        break;
                    case QL_OR_R:
                        quietCriteria = QL_OR_R;
                        break;
                    default:
                        fprintf (stderr, "cbs: illegal value for quiet criteria. Using default: %d.\n",
                                 quietCriteria);
			setQuietCriteria = FALSE;
                        break;
		    }
                break;
	    case 'x':
		mixer_device = optarg;
		break;
            case '?':
            case 'h':
            default:
                usage ();
	}

    /* Enforce default criteria if not set */
    if (mono_mode) {
      trigCriteria = LEVEL_CRIT;
      quietCriteria = QLEVEL_CRIT;
    } else { 
      if (!setTrigCriteria)
	trigCriteria = L_OR_R;
      if (!setQuietCriteria)
	quietCriteria = QL_AND_R;
    }

    /* Update parameters and do range checking */
    WHead.blockAlign = 2 * WHead.channels;
    if (WHead.sample_fq > SAMP_FREQ_MAX ) {
      WHead.sample_fq = SAMP_FREQ_MAX;
    }
    WHead.bytesPerSec = WHead.sample_fq * WHead.blockAlign;
      
    ac -= optind;
    av += optind;
    return (0);

}


void
preMainLoopLogs (void)
{
    /* dump everything if we are debugging */
    if (debugflag) {
        fprintf (logFile, "Sound card '%s' is now open.\n", audio_device);
        fprintf (logFile, "\tchunkSize: %d samples (%d bytes)\n", chunkSize, bytesPerSamp*chunkSize);
        fprintf (logFile, "\tnumChunks: %d\n", numChunks);
        fprintf (logFile, "\ttotalSize: %d\n", totalSize);
        fprintf (logFile, "\tbinSize: %d\n", binSize);
        fprintf (logFile, "\tpreTriggerSize: %d\n", preTriggerSize);
        fprintf (logFile, "\tminDurationSize: %d\n", minDurationSize);
        fprintf (logFile, "\tpostTriggerSize: %d\n", postTriggerSize);
        fprintf (logFile, "\tgapSize: %d\n", gapSize);
/*        fprintf (logFile, "\t%s mode\n", ((WHead.channels == WAV_MONO) ? "Mono" : "Stereo")); */
        fprintf (logFile, "\t%d bits per sample\n", WHead.bitsPerSamp);
#if defined (DO_MAX_FILE_LEN)
        fprintf (logFile, "\tmaxLength: %d\n", maxLength);
#endif
	fprintf (logFile, "--------------------------------------------------\n");
    }

    /* this is useful for analyzing data, always spit out */
    fprintf (logFile, "  BinTime:         %.3f     ms\n", binMSTime);
    fprintf (logFile, "  Freq:            %ld     Hz\n", WHead.sample_fq);
    fprintf (logFile, "  gapTime:         %.3f     s\n", gapTime);
    fprintf (logFile, "  PreTriggerTime:  %.3f     s\n", preTriggerTime);
    fprintf (logFile, "  MinDurationTime: %.3f     s\n", minDurationTime);
    fprintf (logFile, "  PostTriggerTime: %.3f     s\n", postTriggerTime);
    fprintf (logFile, "  Threshold:       %.3f\n", songThresh);
    fprintf (logFile, "  %s mode\n", ((WHead.channels == WAV_MONO) ? "Mono" : "Stereo"));
    if (!mono_mode && trigCriteria != LEVEL_CRIT) {
      fprintf (logFile, "  Threshold L:     %.3f\n", songThreshL);
      fprintf (logFile, "  Threshold R:     %.3f\n", songThreshR);
    }
    if (!mono_mode) {
      switch (trigCriteria) {
      case LEVEL_CRIT:
	fprintf (logFile, "  Trigger criteria: LEVEL\n");
	break;
      case L_OR_R:
	fprintf (logFile, "  Trigger criteria: L_OR_R\n");
	break;
      case L_AND_R:
	fprintf (logFile, "  Trigger criteria: L_AND_R\n");
	break;
      case L:
	fprintf (logFile, "  Trigger criteria: L\n");
	break;
      case R:
	fprintf (logFile, "  Trigger criteria: R\n");
	break;
      case L_NOT_R:
	fprintf (logFile, "  Trigger criteria: L_NOT_R\n");
	break;
      case R_NOT_L:
	fprintf (logFile, "  Trigger criteria: R_NOT_L\n");
	break;
      case L_NOT_R_OR_R_NOT_L:
	fprintf (logFile, "  Trigger criteria: L_NOT_R_OR_R_NOT_L\n");
	break;
      default:
	fprintf (logFile, "  Trigger criteria: UNKNOWN (?!)\n");
	break;
      }
      switch (quietCriteria) {
      case QLEVEL_CRIT:
	fprintf (logFile, "  Quiet criteria: LEVEL\n");
	break;
      case QL_AND_R:
	fprintf (logFile, "  Quiet criteria: L_AND_R\n");
	break;
      case QL:
	fprintf (logFile, "  Quiet criteria: L\n");
	break;
      case QR:
	fprintf (logFile, "  Quiet criteria: R\n");
	break;
      case QL_OR_R:
	fprintf (logFile, "  Quiet criteria: L_OR_R\n");
	break;
      default:
	fprintf (logFile, "  Quiet criteria: UNKNOWN (?!)\n");
	break;
      }

    }
    fprintf (logFile, "--------------------------------------------------\n");
}

