/* wav header file for reading Windows .WAV files */
/* From wavplay utility for Linux by Andre Fuechsel */

// #if defined(osf1)
typedef unsigned int DWORD;     /* 32 bits */
typedef unsigned short WORD;    /* 16 bits */
// #else
// typedef unsigned long DWORD;    /* 32 bits */
// typedef unsigned short WORD;    /* 16 bits */
// #endif

/* See e.g. http://www-ccrma.stanford.edu/CCRMA/Courses/422/projects/WaveFormat/ */
typedef struct {                /* header for WAV-Files */
        char main_chunk[4];     /* 'RIFF' */
        DWORD length;           /* length of file in bytes not including chunkid (main_chunk) and
				   chunksize (length): 36 + data_length */
        char chunk_type[4];     /* 'WAVE' */
        char sub_chunk[4];      /* 'fmt ' */
        DWORD length_chunk;     /* length sub_chunk, always 16 bytes */
        WORD format;            /* always 1 = PCM-Code */
        WORD channels;          /* 1 = Mono, 2 = Stereo */
        DWORD sample_fq;        /* Sample Freq */
        DWORD bytesPerSec;      /* Data per sec */
        WORD blockAlign;      /* bytes/sample, 1=8 bit, 2=16 bit (mono)
                                                 2=8 bit, 4=16 bit (stereo) */
        WORD bitsPerSamp;       /* bits per sample, 8, 12, 16 */
        char data_chunk[4];     /* 'data' */
        DWORD data_length;      /* total length of data in bytes */
} WaveHeader;

#define WAVE_HEADER_SIZE 36     /* all except first two fields above */

