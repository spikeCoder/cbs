/*
 * $Id: init.c,v 1.22 2003/06/26 23:36:51 ds Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h> /* only needed for libc5? BDW */
#include <time.h>
#include <linux/soundcard.h>
#if defined (USE_CURSES)
# include <ncurses.h>
#endif

#include "cbs.h"

char *audio_device = "usbaudio_";
char *mixer_device = "usbaudio_";

void 
initializeValues (void)
{
    strncpy (WHead.main_chunk, "RIFF", 4);
    WHead.length = WAVE_HEADER_SIZE;            /* initially just header */
    strncpy (WHead.chunk_type, "WAVE", 4);
    strncpy (WHead.sub_chunk, "fmt ", 4);
    WHead.length_chunk = 16;
    WHead.format = 1;                           /* Always 1 for PCM */
    WHead.channels = WAV_MONO;
    WHead.sample_fq = 44100;
    WHead.blockAlign = 2 * WHead.channels;                     /* Use 16 bits */
    WHead.bitsPerSamp = 16;
    WHead.bytesPerSec = WHead.sample_fq * WHead.blockAlign;
    strncpy (WHead.data_chunk, "data", 4);
    WHead.data_length = 0;

    return;
}


void 
initializeScreen (void)
{
#if defined (USE_CURSES)
#if 0
  int normal;
#endif

    initscr ();                 /* initialize for curses */
    clear ();
    refresh ();
    cbreak ();
    nodelay (stdscr, TRUE);
    noecho ();
#if 0
    normal = A_NORMAL | ' ';
    bkgdset(normal);
#endif
    if (has_colors()) {
      start_color();
      /* This command needs ncurses >= 5.1 ? */
      use_default_colors();
      init_pair(COLOR_RED, COLOR_RED,     -1);
      init_pair(COLOR_GREEN, COLOR_GREEN,   -1);
      init_pair(COLOR_YELLOW, COLOR_YELLOW,  -1);
      init_pair(COLOR_BLUE, COLOR_BLUE,    -1);
      init_pair(COLOR_CYAN, COLOR_CYAN,    -1);
      init_pair(COLOR_MAGENTA, COLOR_MAGENTA, -1);
    }

    /* show pattern on screen */
    box (stdscr, ACS_VLINE, ACS_HLINE);

    mvprintw (2, 2,  "usbCBS -- Catch Bird Singing ver. %s", VERSION);

    mvprintw (5, 2,  "  Minimum Variance:");
    mvprintw (6, 2,  "  Average Variance:");
    mvprintw (7, 2,  "  Maximum Variance:");
    mvprintw (9, 2,  " Current Threshold:");
    mvprintw (11, 2, "        Peak Value:");
    

    if ( !mono_mode ) {
      chanlock_col =  LEFT_CHAN_COL + ((RIGHT_CHAN_COL - 5 - LEFT_CHAN_COL)/2) -2;
      attron(A_UNDERLINE);
      mvprintw (4, LEFT_CHAN_COL-4,  "Left");
      mvprintw (4, RIGHT_CHAN_COL-5,  "Right");
      attroff(A_UNDERLINE);
      mvprintw(4, chanlock_col, "<-->");
      mvprintw (16, 2,  "l -- Toggle lock of L/R channels");
      mvprintw (17, 2,  "c -- Toggle selection of L/R channel");
    } 

    mvprintw (19, 2, "Free space:");

    mvprintw (13, 45, "* -- Multiply by 10");
    mvprintw (14, 45, "/ -- Divide by 10");
    mvprintw (16, 45, "p -- Threshold  +%d", THRESHSCALE);
    mvprintw (17, 45, "P -- Threshold +%d", THRESHSCALE * 10);
    mvprintw (18, 45, "m -- Threshold  -%d", THRESHSCALE);
    mvprintw (19, 45, "M -- Threshold -%d", THRESHSCALE * 10);
    mvprintw (21, 45, "q -- Quit");
    if (has_colors()) {
      attron(COLOR_PAIR(COLOR_CYAN));
      mvprintw (2, 45,  "usbSoundcard: %s", audio_device);}
      attroff(COLOR_PAIR(COLOR_CYAN));
    refresh();
#endif
    return;
}


void
initFiles (FILE **lfp, char **fnpp, const char *animalNumber)
/* Makes the log file name, opens the file and writes important info */
{
    char tempFileName[256];
    time_t tnum;
    struct tm *ts;
	 int year;

    /* even if this runs for days, we want the file names to always
     * have the same prefix.  tack on the date info to animal identification
     * only once and remember it.
     */
    time (&tnum);
	 /* localtime() returns # of years since 1900 in tm_year. JCR */
    ts = localtime (&tnum);
	 year = 1900 + (int)(ts->tm_year);
    sprintf (tempFileName, "%s.%04d%02d%02d.",
             animalNumber, year, ts->tm_mon + 1, ts->tm_mday);
    if ((*fnpp = (char *) calloc (strlen(tempFileName)+1, sizeof (char))) == NULL) {
        die ("Failed to allocate filenamePrefix of %d chars", strlen(tempFileName)+1);
    }
    strcpy (*fnpp, tempFileName);

    /* now initialize the log file.  can use the prefix info already
     * in place.
     */
    strcat (tempFileName, "log");
    if ((*lfp = fopen (tempFileName, "a")) == NULL) {
        die ("Can't open log file %s", tempFileName);
    } else {
        fprintf (*lfp, "==================================================\n");
        fprintf (*lfp, "CBS Version %s\n", VERSION);
        fprintf (*lfp, "Log file %s\n", tempFileName);
        fprintf (*lfp, ctime(&tnum));
        fprintf (*lfp, "--------------------------------------------------\n");
    }
}


int 
initHardware (void)
{
    int aud;
    int mix;
    int tempInt;


    aud = open ("/dev/dsp", O_RDONLY, 0);
    if (aud == -1) {
        die ("Can't open audio device %s", audio_device);
    }

    mix = open ("/dev/mixer", O_RDWR, 0);
    if (mix == -1) {
        die ("Can't open mixer device %s", mixer_device);
    }

    tempInt = 0;
    if (ioctl (mix, SOUND_MIXER_AGC, &tempInt) == -1) {
      if (errno == EINVAL)
	fprintf(logFile, "Device %s does not have AGC.\n", mixer_device);
      else
	fprintf(logFile, "Can't turn off AGC for device %s.\n", mixer_device);
      if (debugflag) perror("AGC");
    } else if (!tempInt) {
      fprintf(logFile, "Automatic Gain control turned off.\n");
    }
    if (debugflag) fprintf(logFile, "Value of AGC toggle: %d\n", tempInt);
    
    tempInt = 0;
    if (ioctl (mix, SOUND_MIXER_3DSE, &tempInt) == -1) {
      if (errno == EINVAL)
	fprintf(logFile, "Device %s does not have 3DSE.\n", mixer_device);
      else
	fprintf(logFile, "Can't turn off 3D sound effects for device %s.\n", mixer_device);
      if (debugflag) perror("3DSE");

    } else if (!tempInt){
      fprintf(logFile, "3D sound effects turned off.\n");
    }
    if (debugflag) fprintf(logFile, "Value of 3DSE toggle: %d\n", tempInt); 
    
    if (close(mix) == -1) {
      if (debugflag) fprintf(logFile, "Can't close %s.\n", mixer_device); 
    }


    tempInt = (int) WHead.bitsPerSamp;
    if (ioctl (aud, SNDCTL_DSP_SAMPLESIZE, &tempInt) == -1) {
        fprintf (logFile, "ioctl to set sample size to %d failed\n",
                 WHead.bitsPerSamp);
    } else {
        WHead.bitsPerSamp = (WORD) tempInt;
    }
    bytesPerSamp = WHead.bitsPerSamp/8;

    tempInt = SND_CARD_STEREO;
    if (ioctl (aud, SNDCTL_DSP_STEREO, &tempInt) == -1) {
        die ("Can't set %s for %d channels", audio_device, SND_CARD_STEREO);
    }

    tempInt = (int) WHead.sample_fq;
    if (ioctl (aud, SNDCTL_DSP_SPEED, &tempInt) == -1) {
        die ("Can't set %s for sample frequency of %d", audio_device, WHead.sample_fq);
    }
    fprintf(logFile, "speed set to %d\n",tempInt);
    //WHead.sample_fq = (DWORD) tempInt;
    WHead.blockAlign  = bytesPerSamp * WHead.channels;
    WHead.bytesPerSec = WHead.sample_fq * WHead.blockAlign;     /*need to update after change to sample rate */


    /* Use SNDCTL_DSP_SETFRAGMENT to set buffer size. 
       After setting, always check the value chosen. */
    ioctl (aud, SNDCTL_DSP_GETBLKSIZE, &chunkSize);     /* in bytes */
    if (debugflag) {
      fprintf(logFile,"Chunk size in bytes: %d.\n", chunkSize);
    }
    chunkSize /= bytesPerSamp;                    /* now in true samples */

/*    if (chunkSize < 4096 || chunkSize > 65536) { */
    if (chunkSize == -1) {
      die ("Can't get audio chunk buffer size for %s", audio_device);
    } 
/*    else {
      die ("Invalid audio chunk buffer size %d for %s", chunkSize, audio_device);
    } */
    
    return (aud);
}


int 
initNumChunks (void)
{
    float secPerChunk;
    float numChunksFloat;
    int numChunks;


    /*
     * calculate number of chunks in main circular buffer
     * this must be greater than the pretrigger, posttrigger and gap time
     */
    secPerChunk = (float) chunkSize / ((float) WHead.sample_fq * (float) AUDIO_CHANNELS);
    numChunksFloat = (MAX (MAX (preTriggerTime, postTriggerTime), gapTime)) / secPerChunk;
    numChunks = MAX(6, (int) numChunksFloat+4);
    return(numChunks);
}


int 
initChunks (int num, int size)
{
    int bitpos;


    if (debugflag) {
        fprintf (logFile, "Allocating %d contiguous chunk buffers of size %d bytes each\n",
                 num, size * bytesPerSamp);
    }
    if ((mainBuffer = (short *) calloc (size * num, bytesPerSamp)) == NULL) {
        die ("Failed to allocate mainBuffer of %d bytes", size * num * bytesPerSamp);
    }

    /* set up data buffer */
    binSize = (int) (binMSTime * (float) WHead.sample_fq * (float) AUDIO_CHANNELS / 1000.0);
    binMetaSize = (int) (binMSTime * (float) WHead.sample_fq / 1000.0);

    if (debugflag) {
        fprintf (logFile, "Adjusting binSize from %d and binMSTime from %f\n",
                 binSize, binMSTime);
    }

    /* find smallest power of two which is larger than or equal to sPAB */
    for (bitpos=0 ; bitpos < (sizeof (int) * 8) ; bitpos++) {
       if (binSize <= (1 << bitpos)) {
           binSize = (1 << bitpos);
           break;
       }
    }
    binMetaSize = binSize/AUDIO_CHANNELS;
    binMSTime = (float) binMetaSize *1000.0 / (float) WHead.sample_fq;
    if (debugflag) {
        fprintf (logFile, "\tto %d and %f, respectively\n", binSize, binMSTime);
    }

    /* calculate number of pre- and posttrigger samples to include */
    gapSize = (int) (gapTime * (float) WHead.sample_fq * (float) AUDIO_CHANNELS);
    preTriggerSize = (int) (preTriggerTime * (float) WHead.sample_fq * (float) AUDIO_CHANNELS);
    minDurationSize = (int) (minDurationTime * (float) WHead.sample_fq * (float) AUDIO_CHANNELS);
    postTriggerSize = (int) (postTriggerTime * (float) WHead.sample_fq * (float) AUDIO_CHANNELS);
    if (debugflag) {
        fprintf (logFile, "gapSize: %d\npreTriggerSize: %d\nminDurationSize: %d\npostTriggerSize: %d\n",
                 gapSize, preTriggerSize, minDurationSize, postTriggerSize);
    }

    /* calculate total number of samples in buffer */
    totalSize = numChunks * chunkSize;

    /* Allocate stereo analysis buffers */
    if ((bufferL = (short *) calloc (binMetaSize, bytesPerSamp)) == NULL) {
      die ("Failed to allocate bufferL of %d bytes", binMetaSize * bytesPerSamp);
    }
    if ((bufferR = (short *) calloc (binMetaSize, bytesPerSamp)) == NULL) {
      die ("Failed to allocate bufferR of %d bytes", binMetaSize * bytesPerSamp);
    }
    return (0);
}
