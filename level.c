/*
 * $Id: level.c,v 1.4 2003/02/11 23:51:01 ds Exp $
 */

#include "cbs.h"
#include <stdlib.h>

/* Calculate the variance of num values in array data */
float 
calcVariance (short *data, int num, BOOLEAN *ifClip, int *absPeak)
{
    int i;
    float ave = 0.0;
    float var = 0.0;


    *ifClip = FALSE;
    *absPeak = 0;
    if (num < 2) {
        die ("Can't calculate variance for %d points", num);
    }

    /* first get the average (plus the peak) */
    for (i = 0; i < num; i++) {
        if (abs(data[i]) > *absPeak) {
	    *absPeak = abs(data[i]);
	}
        if ((data[i] == MAX_SHORT) || (data[i] == MIN_SHORT)) {
            *ifClip = TRUE;
        }
        ave += (float) data[i];
    }
    ave /= (float) num;

    /* now the variance */
    for (i = 0; i < num; i++) {
        var += (((float) data[i]) - ave) * (((float) data[i]) - ave);
    }

    var /= ((float) (num - 1));
    return (var);
}
