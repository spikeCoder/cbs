#!/usr/bin/perl -w
=head1 NAME

getcbstime - open a given cbs log file and return a printout of the
time stamps for each recorded file in human readable format.


=head1 SYNOPSIS

=over

=item B<getcbstime.pl [ -l, --logfile CBSLOGFILE ]>

=over 2

=item B<[ -f, --file OUTPUTFILE ]>

=item B<[ -h, -?, --help ] [ -M, --man ]>

=back

=back

=head1 OPTIONS

=over

=item B<-l, --logfile CBSLOGFILE>

Set the name of the cbs log file to be analyzed. 
If not specified, the user will be prompted for a name.

=item B<-f, --file OUTPUTFILE>

Specify the file to save timing output to.
If the file exists, it will be appended to.
If not specified the output will be sent to the screen only.

=item B<-h, -?, --help>

Print usage notes.

=item B<-M, --man>

Show man page.

=back

=for comment =head1 USAGE

=head1 DESCRIPTION

Opens a given cbs log file and returns a printout of the
time stamps for each recorded file in human readable format.
The durations of each file are computed and corrected for the 
pre and post time buffer durations. 
The total time of recorded song is also estimated.

=head1 EXAMPLES

Get the onset times and recording durations for each song recorded
during the cbs log file test.20050220.log and save them to the file
test.20050220.times:

    getcbstime.pl -l test.20050220.log -f test.20050220.times

Interactive mode with no results saved:

    getcbstime.pl

=head1 PREREQUISITES

This script requires the requires the
L<Pod::Usage|Pod::Usage>, L<Getopt::Long|Getopt::Long> and
L<POSIX|POSIX> modules.

=for comment =head1 COREQUISITES

=for comment =head1 CAVEATS

=for comment =head1 BUGS

=for comment =head1 SEE ALSO

=head1 AUTHOR

Brian D. Wright E<lt>bdwright@phy.ucsf.eduE<gt>.

=for comment =head1 COPYRIGHT AND DISCLAIMER

=for comment =head1 ACKNOWLEDGEMENTS

=cut

# Idea is to open a given cbs log file and return a printout of the
# time stamps for each recorded file in human readable format
# Blame Brian Wright for this code.

use Pod::Usage;
use Getopt::Long;
use POSIX qw(:time_h); 

Getopt::Long::Configure("bundling","permute","no_getopt_compat");

$opt_help = 0;
$opt_man = 0;
$do_save = 0;

&GetOptions("logfile|l=s" => \$logfile, "file|f=s" => \$outfile, "help|h|?", "man|M") or pod2usage(2);

pod2usage(1) if $opt_help;
pod2usage(-verbose => 2) if $opt_man;

if (!defined($logfile)) {
    print "Enter name of CBS log file: ";
    $logfile = <STDIN>;
}
chomp($logfile);

open LFH, "<$logfile" or die "Can't open file $logfile.\n"; 

if (defined $outfile) {
    chomp($outfile);
    open OFH, ">>$outfile" or die "Can't open file $outfile for writing.\n"; 
    $do_save = 1;
}

%timestamp = ();
%duration = ();
$pretriggertime = 0.0;
$posttriggertime = 0.0;

LINE: while ($line = <LFH>){
    chomp($line);
    if ( $line =~ /PreTriggerTime:\s*(\d+\.?\d*|\.\d+)/ ) {
	$pretriggertime = $1;
	next;
    }
    if ( $line =~ /PostTriggerTime:\s*(\d+\.?\d*|\.\d+)/ ) {
	$posttriggertime = $1;
	next;
    }

    # Only process lines in the "File ... opened at ..." format
    if ( $line =~/^File: (\S+) opened at (\d+\.?\d*|\.\d+)$/ ) {
	$timestamp{$1} = $2;
	next;
    }
    if ( $line =~/^File: (\S+) length (\d+) msec, DONE$/ ) {
	$duration{$1} = $2/1000.0 - ($pretriggertime + $posttriggertime); 
	next;
    }
}

print "\nCBS log contents for $logfile.\n\n";
print OFH "\nCBS log contents for $logfile.\n\n" if $do_save;
print "Pre(Post)TriggerTime: $pretriggertime ($posttriggertime) sec.\n";
print OFH "Pre(Post)TriggerTime: $pretriggertime ($posttriggertime) sec.\n" if $do_save;
print "These times are subtracted from the recording durations.\n";
print OFH "These times are subtracted from the recording durations.\n" if $do_save;

$nfiles = 0;
$duration_total = 0.0;

foreach $cbsfile (sort keys %duration) {
    $nfiles++;
    $duration_total = $duration_total + $duration{$cbsfile};
    # Time stamp in human readable format (hrf)
    $hrftime = POSIX::ctime($timestamp{$cbsfile});
    chomp $hrftime;
    print "File: $cbsfile, Recorded: $hrftime, Duration: $duration{$cbsfile} sec\n";
    print OFH "File: $cbsfile, Recorded: $hrftime, Duration: $duration{$cbsfile} sec\n" if $do_save;
}

print "\n";
print "Total duration for $nfiles files: $duration_total sec.\n";
print OFH "\n" if $do_save;
print OFH "Total duration for $nfiles files: $duration_total sec.\n" if $do_save;



	      
		  
