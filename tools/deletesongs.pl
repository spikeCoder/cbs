#!/usr/bin/perl -w

# Song file deletion script for use with soundscreen.
# More cautious than axe.
# Blame Brian Wright for this code.
  
print "Enter the name of the source directory: ";
$basedir = <STDIN>;
chomp($basedir);
print "Enter name of status file: ";
$statname = <STDIN>;
chomp($statname);
$statfile = join "/", $basedir, $statname;

open SFH, "<$statfile" or die "Can't open file $statfile.\n"; 
$i = 0;

LINE: while ($line = <SFH>){
    chomp($line);
    next LINE if ($line =~ /^\s*#/);  # skip comments
    next LINE if ($line =~ /^$/);     # skip blank lines
    ($song, $status) = split " ", $line, 2;
    if ($status =~ /DELETE/) {
	$i++;
	if ($i == 1){
	    print "\nFiles to be deleted: \n";
	}
	print "$song $status\n";
	$songfile[$i-1] = join "/", $basedir, $song;
    }		  
}
$numdelsongs = $i;

print "Are you SURE you want to delete all these files? (y/[n]) ";
$ans = <STDIN>;
chomp($ans);
if ($ans =~ /(y|Y).*/){
    print "Deleting files...\n";
    for ($i=0; $i < $numdelsongs; $i++) {
	print "$songfile[$i]\n";
    }
    unlink @songfile;
    print "\nTotal of $numdelsongs files deleted.\n"
}
else{
    print "\nNo files were deleted.\n"
}

close SFH;
print "Done.\n";
