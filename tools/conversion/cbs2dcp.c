/*
 * cbs2dcp - convert cbs (RIFF/WAVE) files to dcp format
 *
 * --jrw  13may95
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wordsizes.h"
#include "wave.h"
#include "swap.h"


void usage (argc, argv)
int argc;
char **argv;
{
    fprintf (stderr, "Usage: %s <infile> <outfile>\n", argv[0]);
    exit (1);
}

main (argc, argv)
int argc;
char *argv[];
{
    char infile[256], outfile[256];
    S16BIT *waveform;
    int numsamples;
    int i;
    FILE *infp, *outfp;
    WaveHeader head;

    if (argc <= 2) {
	usage (argc, argv);
    }
    strcpy (infile, argv[1]);
    strcpy (outfile, argv[2]);

    if ((infp = fopen (infile, "rb")) == NULL) {
	fprintf (stderr, "Error opening input file '%s'", infile);
	perror("");
	exit (1);
    }
    if ((outfp = fopen (outfile, "wb")) == NULL) {
	fprintf (stderr, "Error opening output file '%s'", outfile);
	perror("");
	exit (1);
    }
    if (fread (&head, sizeof (WaveHeader), 1, infp) != 1) {
	fprintf (stderr, "Error reading header from file '%s'", infile);
	perror("");
	exit (1);
    }

#ifdef bigendian  /* motorola, sparc, etc */
    swap_dword (&head.length);
    swap_dword (&head.length_chunk);
    swap_word  (&head.format);
    swap_word  (&head.channels);
    swap_dword (&head.sample_fq);
    swap_dword (&head.bytesPerSec);
    swap_word  (&head.bytesPerSamp);
    swap_word  (&head.bitsPerSamp);
    swap_dword (&head.data_length);
#endif

    if (head.format != 1 || head.channels != 1 || head.bytesPerSamp != 2) {
	fprintf(stderr, "Dunno how to deal with this RIFF/WAVE format\n");
	exit (1);
    }

    waveform = (S16BIT *)malloc(head.data_length);
    if (waveform == NULL) {
	fprintf(stderr, "Asking for %d bytes", head.data_length);
	perror("");
	exit(1);
    }
    numsamples = head.data_length / 2;
    if (fread(waveform, 2, numsamples, infp) != numsamples) {
	fprintf(stderr, "Error reading input file\n");
	perror("");
	exit(1);
    }
    /*
     * stuff from WAVE file comes in little-endian
     * stuff for dcp is big-endian
     * thus we have to byte-swap no matter what machine we are running on
     */
    for (i=0 ; i<numsamples ; i++) {
	swap_s16bit(&waveform[i]);
    }

    printf ("freq=%d numsamples=%d\n", head.sample_fq, numsamples);

    /*
     * MAJOR BUG? - songed rejects any file which is not 32000 Hz
     * not sure if dcp is similarly broken
     */
    /* fprintf (outfp, "AD_FREQ: %d Hz\n", head.sample_fq); */
fprintf(stderr, "WARNING: saying freq of 32000 HZ -- NO data conversion however\n");
fprintf (outfp, "AD_FREQ: 32000 Hz\n");

    i=numsamples;
#ifdef littleendian  /* alpha, intel, etc */
    swap_s32bit(&i);
#endif
    fwrite (&i, sizeof (int), 1, outfp);
    if (fwrite (waveform, 2, numsamples, outfp) != numsamples) {
	fprintf (stderr, "Error writing output file.\n");
	perror ("");
	exit (1);
    }
    fclose (outfp);
    fclose (infp);
    exit (0);
}
