/*
 * cbs2dcp - convert cbs (RIFF/WAVE) files to dcp format
 *
 * --jrw  13may95
 *
 * ---bdw 5/20/99 modified to truncate files more than 8sec long
 */


#include <stdio.h>
#include <stdlib.h>
#include "wordsizes.h"
#include "wave.h"
#include "swap.h"


void usage (argc, argv)
int argc;
char **argv;
{
    fprintf (stderr, "Usage: %s <infile> <outfile>\n", argv[0]);
    exit (1);
}

main (argc, argv)
int argc;
char *argv[];
{
    char infile[256], outfile[256];
    S16BIT *waveform;
    int numsamples;
    int maxsamples;
    unsigned char bwf;
    int i;
    FILE *infp, *outfp;
    WaveHeader head;

    if (argc <= 2) {
	usage (argc, argv);
    }
    strcpy (infile, argv[1]);
    strcpy (outfile, argv[2]);

    if ((infp = fopen (infile, "rb")) == NULL) {
	fprintf (stderr, "Error opening input file '%s'", infile);
	perror("");
	exit (1);
    }
    if ((outfp = fopen (outfile, "wb")) == NULL) {
	fprintf (stderr, "Error opening output file '%s'", outfile);
	perror("");
	exit (1);
    }
    if (fread (&head, sizeof (WaveHeader), 1, infp) != 1) {
	fprintf (stderr, "Error reading header from file '%s'", infile);
	perror("");
	exit (1);
    }

#ifdef bigendian  /* motorola, sparc, etc */
    swap_dword (&head.length);
    swap_dword (&head.length_chunk);
    swap_word  (&head.format);
    swap_word  (&head.channels);
    swap_dword (&head.sample_fq);
    swap_dword (&head.bytesPerSec);
    swap_word  (&head.bytesPerSamp);
    swap_word  (&head.bitsPerSamp);
    swap_dword (&head.data_length);
#endif

    if (head.format != 1 || head.channels != 1 || (head.bytesPerSamp != 2 && head.bytesPerSamp != 1) ) {
	fprintf(stderr, "Dunno how to deal with this RIFF/WAVE format\n");
	exit (1);
    }

    if (head.bytesPerSamp == 1) {
      printf("Converting 8 bit to 16 bit data\n.");
      waveform = (S16BIT *)malloc(2*head.data_length);
      if (waveform == NULL) {
	fprintf(stderr, "Asking for %d bytes", 2*head.data_length);
	perror("");
	exit(1);
      }
    } else {
      waveform = (S16BIT *)malloc(head.data_length);
      if (waveform == NULL) {
	fprintf(stderr, "Asking for %d bytes", head.data_length);
	perror("");
	exit(1);
      }
    }

    numsamples = head.data_length/head.bytesPerSamp;
    maxsamples = 10*head.sample_fq; /* dcp/songed can only handle 10 sec of data? */
    if (numsamples > maxsamples) {
      printf("Truncating file %s from %d to 10 seconds.\n", infile, numsamples/head.sample_fq);
      numsamples = maxsamples;
    }

    if (head.bytesPerSamp == 1) {
      for (i=0 ; i<numsamples ; i++) {
	if (fread(&bwf, sizeof(unsigned char), 1, infp) != 1) {
	  fprintf(stderr, "Error reading input file\n");
	  perror("");
	  exit(1);
	}
	waveform[i] = bwf - 128;
	if (i < 1000) {
	  printf("Sample %d is %d\n.", i, waveform[i]);
	}
      }
    } else {
      if (fread(waveform, sizeof(S16BIT), numsamples, infp) != numsamples) {
	fprintf(stderr, "Error reading input file\n");
	perror("");
	exit(1);
      }
    }
    /*
     * stuff from WAVE file comes in little-endian
     * stuff for dcp is big-endian
     * thus we have to byte-swap no matter what machine we are running on
     */
    for (i=0 ; i<numsamples ; i++) {
	swap_s16bit(&waveform[i]);
    }

    printf ("freq=%d numsamples=%d\n", head.sample_fq, numsamples);

    /*
     * MAJOR BUG? - songed rejects any file which is not 32000 Hz
     * not sure if dcp is similarly broken
     */
    /* fprintf (outfp, "AD_FREQ: %d Hz\n", head.sample_fq); */
fprintf(stderr, "WARNING: saying freq of 32000 HZ -- NO data conversion however\n");
fprintf (outfp, "AD_FREQ: 32000 Hz\n");

    i=numsamples;
#ifdef littleendian  /* alpha, intel, etc */
    swap_s32bit(&i);
#endif
    fwrite (&i, sizeof (int), 1, outfp);
    if (fwrite (waveform, 2, numsamples, outfp) != numsamples) {
	fprintf (stderr, "Error writing output file.\n");
	perror ("");
	exit (1);
    }
    fclose (outfp);
    fclose (infp);
    exit (0);
}
