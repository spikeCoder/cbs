/*
 * $Id: die.c,v 1.1 2003/04/12 01:00:38 bdwright Exp $
 */

#include "cbs.h"
#include "stdarg.h"
#if defined (USE_CURSES)
# include <ncurses.h>
#endif


#define LOGBUFSIZE 1024

void
die(char *fmt, ...)
{
    va_list args;
    static char mess[LOGBUFSIZE]; /* static, cause if we die due to no memory...  */

    /* first, clean up the screen if we've cursed it */
#if defined (USE_CURSES)
    clear ();
    refresh ();
    nocbreak ();
    endwin ();
#endif

    /* then format message and spit it out */
    va_start (args, fmt);
    vsprintf (mess, fmt, args);
    va_end (args);
    perror (mess);

    /* then die */
    exit (-1);
}
