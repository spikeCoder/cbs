/* wav header file for reading Windows .WAV files */
/* From wavplay utility for Linux by Andre Fuechsel */

#ifndef WAVE_H_INC
#define WAVE_H_INC

#include "wordsizes.h"

typedef struct {                /* header for WAV-Files */
        char main_chunk[4];     /* 'RIFF' */
        DWORD length;           /* length of file */
        char chunk_type[4];     /* 'WAVE' */
        char sub_chunk[4];      /* 'fmt ' */
        DWORD length_chunk;     /* length sub_chunk, always 16 bytes */
        WORD format;            /* always 1 = PCM-Code */
        WORD channels;          /* 1 = Mono, 2 = Stereo */
        DWORD sample_fq;        /* Sample Freq */
        DWORD bytesPerSec;      /* Data per sec */
        WORD bytesPerSamp;      /* bytes/sample, 1=8 bit, 2=16 bit (mono)
                                                 2=8 bit, 4=16 bit (stereo) */
        WORD bitsPerSamp;       /* bits per sample, 8, 12, 16 */
        char data_chunk[4];     /* 'data' */
        DWORD data_length;      /* length of data */
} WaveHeader;

#define WAVE_HEADER_SIZE 36     /* all except last two fields above */

#endif /* WAVE_H_INC */
