/*************************************************************************
From lewicki@cyboscine.etho.caltech.edu
Date: Fri, 12 May 95 14:33:56 PDT
From: Michael Lewicki <lewicki@cyboscine.etho.caltech.edu>
To: jwright@phy.ucsf.edu
Cc: ajd@phy.ucsf.edu
Subject: dcp song file format

The dcp sound file format is raw 16 bit binary with the first line:
'AD_FREQ: 32000Hz\n' (or whatever the sampling freq is) and the next
byte and int indicating how many samples follow.  This strange format
is from 'songed' and old data collection software.

I've included a program to convert raw files to dcp song files.
Let me know if you have any questions.
Mike
*************************************************************************/

/* File: raw2dcp.c */
/* A program to convert raw binary sounds files into dcp format. */
/* The sampling frequency is set to 32kHz. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(ac, av)
     int ac;
     char **av;
{
  fprintf(stderr,"\
convert raw binary sounds files into dcp format.\n\
\n\
Usage: %s <rawfile> <newfile>\n", av[0]);
}
    
main(ac, av)
    int ac;
    char **av;
{
    char rawfile[256], newfile[256];
    short *waveform;
    int len, n, chunksize=1024;
    FILE *infp, *outfp;

    if (ac <= 2) {
	usage(ac,av);
	exit(1);
    }

    strcpy(rawfile, av[1]);
    strcpy(newfile, av[2]);

    if ((infp = fopen(rawfile, "rb")) == NULL) {
	fprintf(stderr,"Error opening raw binary file \"%s\"",rawfile);
	exit(1);
    }

    waveform = (short *)malloc(chunksize*sizeof(short));
    len = 0;

    while ((n = fread(&waveform[len], sizeof(short), chunksize, infp))
	   == chunksize) {
	len += chunksize;
	waveform = (short *)realloc(waveform, (len + chunksize)*sizeof(short));
    }

    if (n > 0) {
	len += n;
    } else {
	fprintf(stderr,"Error reading waveform file.\n");
    }

    fclose(infp);

    if ((outfp = fopen(newfile, "wb")) == NULL) {
	fprintf(stderr,"Error opening dcp song file \"%s\"",newfile);
	exit(1);
    }
    
    fprintf(outfp,"AD_FREQ: 32000 Hz\n");
    fwrite(&len, sizeof(int), 1, outfp);
    if (fwrite(waveform, sizeof(short), len, outfp) != len) {
	fprintf(stderr,"Error writing songfile.\n");
	fclose(outfp);
	exit(1);
    }
    fclose(outfp);
    exit(0);
}
