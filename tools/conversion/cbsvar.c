/* calculate variance of cbs data
 *
 * $Id: cbsvar.c,v 1.2 2005/11/22 07:20:11 bdwright Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "cbs.h"
#include "wordsizes.h"
#include "wave.h"
#include "swap.h"

#undef ARGOPTIONS		/* wipe out setting from cbs */
#define ARGOPTIONS "b:"		/* put in our own */
float binMSTime = 20.0;
#define FREQUENCY 44100

int 
main (int argc, char *argv[], char *envp[])
{
    char *filenamePrefix;
    S16BIT *waveform;
    int i;
    int j;
    int n;
    FILE *infp;
    FILE *outfp;
    WaveHeader head;
    int binSize;
    int bitpos;
    char outfilename[256];
    int numSamples;
    int numBins;
    int clipped;
    int peak;
    struct stat statbuf;


    parseCommandLine (argc, argv);

    /*
     * ask for memory to hold two bins at FREQUENCY sample rate
     * should be plenty of memory
     */
fprintf(stderr, "binMSTime=%f FREQUENCY=%d malloc=%ld\n",
	binMSTime, FREQUENCY, (long)(binMSTime * FREQUENCY * 2 / 1000.0));

    waveform = (S16BIT *)malloc((long)(binMSTime * FREQUENCY * 2 / 1000.0));
    if (waveform == NULL) {
	fprintf(stderr, "Asking for %ld bytes\n", (long)(binMSTime * FREQUENCY * 2 / 1000.0));
	perror("");
	exit(1);
    }

    i = 1;
    while (argv[i]) {
	if (stat(argv[i], &statbuf) != 0) {
	    fprintf (stderr, "No such file: '%s'\n", argv[i]);
	    i++;
	    continue;
	}
	if ((infp = fopen(argv[i], "r")) == NULL) {
	    fprintf (stderr, "Error opening file: '%s'\n", argv[i]);
	    i++;
	    continue;
	}
	if (fread(&(head), sizeof(WaveHeader), 1, infp) != 1) {
	    fprintf (stderr, "Error reading file: '%s'\n", argv[i]);
	    perror("fread header");
	    i++;
	    continue;
	}

#ifdef bigendian  /* motorola, sparc, etc */
	swap_dword (&head.length);
	swap_dword (&head.length_chunk);
	swap_word  (&head.format);
	swap_word  (&head.channels);
	swap_dword (&head.sample_fq);
	swap_dword (&head.bytesPerSec);
	swap_word  (&head.bytesPerSamp);
	swap_word  (&head.bitsPerSamp);
	swap_dword (&head.data_length);
#endif

	binSize = (int)(binMSTime * head.sample_fq / 1000.0);
	/* find smallest power of two which is larger than or equal */
	for (bitpos=0 ; ; bitpos++) {
	    if (binSize <= (1 << bitpos)) {
		binSize = (1 << bitpos);
		break;
	    }
	}

	strcpy (outfilename, argv[i]);
	strcat (outfilename, ".var");
	if ((outfp = fopen(outfilename, "w")) == NULL) {
	    fprintf (stderr, "Error opening file: '%s'\n", outfilename);
	    i++;
	    continue;
	}

	numSamples = head.data_length / 2;
	numBins = numSamples / binSize;
	for (j=0 ; j<numBins ; j++) {
	    if (fread(waveform, 2, binSize, infp) != binSize) {
		fprintf(stderr, "Error reading file: '%s'\n", argv[i]);
		perror("fread waveform");
		continue;
	    }
#ifdef bigendian /* motorola, sparc, etc */
	    for (n=0 ; n<binSize ; n++) {
		swap_s16bit(&waveform[n]);
	    }
#endif
	    fprintf (outfp, "%f\n", calcVariance(waveform, binSize, &clipped, &peak));
	}
	fclose(outfp);
	i++;
    }

    exit (0);
}



/* shamelessly stolen from example in getopt(3) man page */
int 
parseCommandLine (int ac, char *av[])
{
    int tempInt;
    int ch;
    float tempFloat;
    extern int optind;
    extern char *optarg;


    while ((ch = getopt (ac, av, ARGOPTIONS)) != EOF)
        switch (ch) {
            case 'b':
                tempFloat = atof (optarg);
                if (tempFloat <= 0.0) {
                    fprintf (stderr, "cbsvar: illegal value '%f' for binMSTime. Using default: %f.\n",
                             tempFloat, binMSTime);
                } else {
                    binMSTime = tempFloat;
                }
                break;
        }
    ac -= optind;
    av += optind;
    return (0);
}
