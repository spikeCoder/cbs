#ifndef IEEE_H_INC
#define IEEE_H_INC

void ConvertToIeeeExtended (double num, char *bytes);
double ConvertFromIeeeExtended (unsigned char *bytes);

#endif /* IEEE_H_INC */
