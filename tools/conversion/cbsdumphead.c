/*
 * cbsdumphead - dump header info from a cbs file
 *
 * --jrw  17may95
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wordsizes.h"
#include "wave.h"
#include "swap.h"


main (argc, argv)
int argc;
char *argv[];
{
    char infile[256];
    FILE *infp;
    WaveHeader head;

    if (argc != 2) {
        fprintf (stderr, "Usage: %s <infile>\n", argv[0]);
	exit (1);
    }
    strcpy (infile, argv[1]);

    infp = fopen(infile, "r");
    if (infp == NULL) {
	/* error */
	perror("fopen");
	abort();
    }
    if (fread(&(head), sizeof(WaveHeader), 1, infp) != 1) {
	/* error */
	perror("fread header");
	abort();
    }
    printf ("\tmain_chunk  '%c%c%c%c'\n", head.main_chunk[0],
				    head.main_chunk[1],
				    head.main_chunk[2],
				    head.main_chunk[3]);
    printf ("\tlength       %u\n", head.length);
    printf ("\tchunk_type  '%c%c%c%c'\n", head.chunk_type[0],
				    head.chunk_type[1],
				    head.chunk_type[2],
				    head.chunk_type[3]);
    printf ("\tsub_chunk   '%c%c%c%c'\n", head.sub_chunk[0],
				    head.sub_chunk[1],
				    head.sub_chunk[2],
				    head.sub_chunk[3]);
    printf ("\tlength_chunk %u\n", head.length_chunk);
    printf ("\tformat       %u\n", (unsigned int)head.format);
    printf ("\tchannels     %u\n", (unsigned int)head.channels);
    printf ("\tsample_fq    %u\n", head.sample_fq);
    printf ("\tbytesPerSec  %u\n", head.bytesPerSec);
    printf ("\tbytesPerSamp %u\n", (unsigned int)head.bytesPerSamp);
    printf ("\tbitsPerSamp  %u\n", (unsigned int)head.bitsPerSamp);
    printf ("\tdata_chunk  '%c%c%c%c'\n", head.data_chunk[0],
				    head.data_chunk[1],
				    head.data_chunk[2],
				    head.data_chunk[3]);
    printf ("\tdata_length  %u\n", head.data_length);
#ifdef bigendian
    printf ("swapping bytes to accomodate differences in hardware\n");
    swap_dword(&head.length);
    swap_dword(&head.length_chunk);
    swap_word(&head.format);
    swap_word(&head.channels);
    swap_dword(&head.sample_fq);
    swap_dword(&head.bytesPerSec);
    swap_word(&head.bytesPerSamp);
    swap_word(&head.bitsPerSamp);
    swap_dword(&head.data_length);

    printf ("\tmain_chunk  '%c%c%c%c'\n", head.main_chunk[0],
				    head.main_chunk[1],
				    head.main_chunk[2],
				    head.main_chunk[3]);
    printf ("\tlength       %u\n", head.length);
    printf ("\tchunk_type  '%c%c%c%c'\n", head.chunk_type[0],
				    head.chunk_type[1],
				    head.chunk_type[2],
				    head.chunk_type[3]);
    printf ("\tsub_chunk   '%c%c%c%c'\n", head.sub_chunk[0],
				    head.sub_chunk[1],
				    head.sub_chunk[2],
				    head.sub_chunk[3]);
    printf ("\tlength_chunk %u\n", head.length_chunk);
    printf ("\tformat       %u\n", (unsigned int)head.format);
    printf ("\tchannels     %u\n", (unsigned int)head.channels);
    printf ("\tsample_fq    %u\n", head.sample_fq);
    printf ("\tbytesPerSec  %u\n", head.bytesPerSec);
    printf ("\tbytesPerSamp %u\n", (unsigned int)head.bytesPerSamp);
    printf ("\tbitsPerSamp  %u\n", (unsigned int)head.bitsPerSamp);
    printf ("\tdata_chunk  '%c%c%c%c'\n", head.data_chunk[0],
				    head.data_chunk[1],
				    head.data_chunk[2],
				    head.data_chunk[3]);
    printf ("\tdata_length  %u\n", head.data_length);
#endif
}
