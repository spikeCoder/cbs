/* aiff header file for reading Mac .AIFF files */

#ifndef AIFF_H_INC
#define AIFF_H_INC

#include "wordsizes.h"

typedef struct {                /* header for WAV-Files */
    char main_chunk[4];		/* 'FORM' */
    S32BIT length;		/* file length in bytes, less these 8 bytes */
    char form_type[4];		/* 'AIFF' - audio interchange file format */
    char common_chunk[4];	/* 'COMM' */
    S32BIT common_length;	/* 18 bytes */
    S16BIT common_channels;	/* # audio channels */
    U32BIT common_sampframes;	/* # sample_frames (1/2 of tot if stereo) */
    S16BIT common_sampsize;	/* # bits/sample */
    F80BIT common_samprate;	/* # sample_frames/sec */
    char sound_chunk[4];	/* 'SSND' - sampled sound */
    S32BIT sound_length;	/* size of sound in bytes + 8 for next bytes */
    S32BIT sound_offset;	/* offset - set to 0 */
    S32BIT sound_blocksize;	/* block_size - set to 0 */
} AIFFHeader;

#define AIFF_HEADER_SIZE 54	/* size in bytes of header (no padding!) */

/*
 * NOTE - spec calls for an IEEE 754 80-bit floating point number
 * we fake it out with an IEEE 754 64-bit floating point number with
 * two NULL bytes appended
 */

#endif /* AIFF_H_INC */
