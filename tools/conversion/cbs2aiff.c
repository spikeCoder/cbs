/*
 * cbs2aiff - convert cbs (RIFF/WAVE) files
 *            to aiff (Amiga Audio Interchange File Format)
 *
 * --jrw  23may95
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wordsizes.h"
#include "wave.h"
#include "aiff.h"
#include "swap.h"
#include "ieee.h"


void usage (argc, argv)
int argc;
char **argv;
{
    fprintf (stderr, "Usage: %s <infile> <outfile>\n", argv[0]);
    exit (1);
}

int main (argc, argv)
int argc;
char *argv[];
{
    char infile[256], outfile[256];
    S16BIT *waveform;
    int numsamples;
    int i;
    FILE *infp, *outfp;
    WaveHeader wave;
    AIFFHeader aiff;

    if (argc <= 2) {
	usage (argc, argv);
    }
    strcpy (infile, argv[1]);
    strcpy (outfile, argv[2]);

    if ((infp = fopen (infile, "rb")) == NULL) {
	fprintf (stderr, "Error opening input file '%s'", infile);
	perror("");
	exit (1);
    }
    if ((outfp = fopen (outfile, "wb")) == NULL) {
	fprintf (stderr, "Error opening output file '%s'", outfile);
	perror("");
	exit (1);
    }
    if (fread (&wave, sizeof (WaveHeader), 1, infp) != 1) {
	fprintf (stderr, "Error reading header from file '%s'", infile);
	perror("");
	exit (1);
    }

#ifdef bigendian  /* motorola, sparc, etc */
    swap_dword (&wave.length);
    swap_dword (&wave.length_chunk);
    swap_word  (&wave.format);
    swap_word  (&wave.channels);
    swap_dword (&wave.sample_fq);
    swap_dword (&wave.bytesPerSec);
    swap_word  (&wave.bytesPerSamp);
    swap_word  (&wave.bitsPerSamp);
    swap_dword (&wave.data_length);
#endif

    if (wave.format != 1 || wave.channels != 1 || wave.bytesPerSamp != 2) {
	fprintf(stderr, "Dunno how to deal with this RIFF/WAVE format\n");
	exit (1);
    }

    waveform = (S16BIT *)malloc(wave.data_length);
    if (waveform == NULL) {
	fprintf(stderr, "Asking for %ld bytes", wave.data_length);
	perror("");
	exit(1);
    }
    numsamples = wave.data_length / 2;
    if (fread(waveform, 2, numsamples, infp) != numsamples) {
	fprintf(stderr, "Error reading input file\n");
	perror("");
	exit(1);
    }
    /*
     * stuff from WAVE file comes in little-endian
     * stuff to aiff is big-endian
     * thus we have to byte-swap no matter what machine we are running on
     */
    for (i=0 ; i<numsamples ; i++) {
	swap_s16bit(&waveform[i]);
    }

    printf ("freq=%ld numsamples=%d\n", wave.sample_fq, numsamples);

    aiff.main_chunk[0] = 'F';
    aiff.main_chunk[1] = 'O';
    aiff.main_chunk[2] = 'R';
    aiff.main_chunk[3] = 'M';
    aiff.length = wave.data_length + AIFF_HEADER_SIZE - 8;
    aiff.form_type[0] = 'A';
    aiff.form_type[1] = 'I';
    aiff.form_type[2] = 'F';
    aiff.form_type[3] = 'F';
    aiff.common_chunk[0] = 'C';
    aiff.common_chunk[1] = 'O';
    aiff.common_chunk[2] = 'M';
    aiff.common_chunk[3] = 'M';
    aiff.common_length = 18;
    aiff.common_channels = wave.channels;
    aiff.common_sampframes = numsamples;
    aiff.common_sampsize = wave.bitsPerSamp;
    ConvertToIeeeExtended(wave.sample_fq, &(aiff.common_samprate.bytes[0]));
    aiff.sound_chunk[0] = 'S';
    aiff.sound_chunk[1] = 'S';
    aiff.sound_chunk[2] = 'N';
    aiff.sound_chunk[3] = 'D';
    aiff.sound_length = wave.data_length + 8;
    aiff.sound_offset = 0;
    aiff.sound_blocksize = 0;

#ifdef SEE_BYTES_IN_EXTENDED_FLOAT
    printf("bytes= %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
	   (int)aiff.common_samprate.bytes[0],
	   (int)aiff.common_samprate.bytes[1],
	   (int)aiff.common_samprate.bytes[2],
	   (int)aiff.common_samprate.bytes[3],
	   (int)aiff.common_samprate.bytes[4],
	   (int)aiff.common_samprate.bytes[5],
	   (int)aiff.common_samprate.bytes[6],
	   (int)aiff.common_samprate.bytes[7],
	   (int)aiff.common_samprate.bytes[8],
	   (int)aiff.common_samprate.bytes[9]);
#endif

#ifdef littleendian  /* alpha, intel, etc */
    swap_s32bit(&aiff.length);
    swap_s32bit(&aiff.common_length);
    swap_s16bit(&aiff.common_channels);
    swap_u32bit(&aiff.common_sampframes);
    swap_s16bit(&aiff.common_sampsize);
    /* swap_f64bit(&aiff.common_samprate); */
    swap_s32bit(&aiff.sound_length);
#endif

    /*
     * i'd like to write the entire header in a single blast, but
     * i can't prevent the damn sun compiler from shoving extra
     * padding in the structure to force alignment
     */
    fwrite (aiff.main_chunk, sizeof (aiff.main_chunk), 1, outfp);
    fwrite (&(aiff.length), sizeof (aiff.length), 1, outfp);
    fwrite (aiff.form_type, sizeof (aiff.form_type), 1, outfp);
    fwrite (aiff.common_chunk, sizeof (aiff.common_chunk), 1, outfp);
    fwrite (&(aiff.common_length), sizeof (aiff.common_length), 1, outfp);
    fwrite (&(aiff.common_channels), sizeof (aiff.common_channels), 1, outfp);
    fwrite (&(aiff.common_sampframes), sizeof (aiff.common_sampframes), 1, outfp);
    fwrite (&(aiff.common_sampsize), sizeof (aiff.common_sampsize), 1, outfp);
    fwrite (&(aiff.common_samprate), sizeof (aiff.common_samprate), 1, outfp);
    fwrite (aiff.sound_chunk, sizeof (aiff.sound_chunk), 1, outfp);
    fwrite (&(aiff.sound_length), sizeof (aiff.sound_length), 1, outfp);
    fwrite (&(aiff.sound_offset), sizeof (aiff.sound_offset), 1, outfp);
    fwrite (&(aiff.sound_blocksize), sizeof (aiff.sound_blocksize), 1, outfp);
    if (fwrite (waveform, 2, numsamples, outfp) != numsamples) {
	fprintf (stderr, "Error writing output file.\n");
	perror ("");
	exit (1);
    }
    fclose (outfp);
    fclose (infp);
    exit (0);
}
