/*
check-var.c

check variables from a log file generated with VARIABLE_DEBUG turned on.
see if we can find any instance where startwrite == endwrite

USAGE:
    grep ^qs test.951009.log | ./check-var

09oct95 --jrw
*/

#include <stdio.h>

main()
{
    int sw, ew;

    while (scanf("=0 =%d =%d\n", &sw, &ew) == 2) {
	printf(".");
	if (sw == ew) {
	    printf("\nshit, %d\n", sw);
	}
    }
    printf("\n");
}
