# Makefile for Catch Bird Singing (cbs)

VERSION = 2

# Configure the following to match your system

# which compiler to use
CC = gcc

prefix = /usr/local

# To have debugging info in the file, use -g
# To have verbose debugging messages in the log file use -DDebug
# Note that you can do this from the command line with the -d option at runtime
#DEBUGFLAGS = -g -DDEBUG
#DEBUGFLAGS = -g
DEBUGFLAGS = -O2
#DEBUGFLAGS = -O2 -g -m486 -DSTATE_DEBUG
#DEBUGFLAGS = -O2 -g -m486 -DWRITE_DEBUG
#DEBUGFLAGS = -O2 -g -m486 -DWRITE_DEBUG -DVARIABLE_DEBUG
#DEBUGFLAGS = -O2 -g -m486 -DZERO_DEBUG
#DEBUGFLAGS = -O2 -g -m486 -DVARIANCE_DEBUG
#DEBUGFLAGS = -O2 -g -m486 -DSTATE_DEBUG -DVARIANCE_DEBUG

# Crude curses display while the program is running.
USE_CURSES = -DUSE_CURSES -I/usr/include/ncurses
#USE_CURSES = 

# if you have a special place for your curses library
#CURSES_LIBDIR = /usr/local/lib
#TERMCAP_LIBDIR = /usr/local/lib
CURSES_LIBS = -lncurses -ltermcap

# Where to install
BINDIR = $(DESTDIR)$(prefix)/bin

# You should not need to change anything below here
CFLAGS = -Wall -Wstrict-prototypes $(DEBUGFLAGS) $(USE_CURSES) -DVERSION=\"$(VERSION)\"
LIBS = -L$(CURSES_LIBDIR) -L$(TERMCAP_LIBDIR) $(CURSES_LIBS) -lm -lc

#### build cbs ####

SRCS = acquire.c init.c disk.c die.c level.c cbs.c
OBJS = acquire.o init.o disk.o die.o level.o cbs.o
PROG = cbs

all:		$(PROG)
	$(MAKE) -C tools/conversion

$(PROG):	$(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS) $(LIBS) 

-include .depend

#### house cleaning ####

.depend depend:
	for i in $(SRCS);do $(CPP) -M $(CFLAGS) $$i;done > .tmpdepend
	mv .tmpdepend .depend

install:	$(PROG)
	install -d $(BINDIR)
	install -m755 $(PROG) $(BINDIR)
	install -m755 usbcbs $(BINDIR)
	install -m755 tools/cbs_axe $(BINDIR)
	install -m755 tools/deletesongs.pl $(BINDIR)
	install -m755 tools/getcbstime.pl $(BINDIR)
	install -m755 tools/cbs_rm_unviewed $(BINDIR)
	install -m755 tools/cbs_save $(BINDIR)
	install -m755 tools/conversion/cbsdumphead $(BINDIR)

clean:
	rm -f .depend core *.o
	rm -f cbs
	$(MAKE) -C tools/conversion clean


