/* Catch Bird Singing
 *
 * $Id: acquire.c,v 1.12 2003/12/12 23:12:37 ds Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <values.h>
#include <linux/soundcard.h>
#if defined (USE_CURSES)
# include <ncurses.h>
#endif

#include "cbs.h"


void 
getChunk (int audioDevice, short *buffer, int n)
{
    int total = 0;
    int thisRead = 0;
    int leftToRead = n;

    while (total < n) {
        thisRead = read (audioDevice, &(buffer[total]), leftToRead);
        if (thisRead < 0) {
            die ("ARRRGGGHHHH! error reading from sound card");
        }
        total += thisRead;
        leftToRead -= thisRead;
    }
}


void 
getSong (int audioDevice, char *filenamePrefix)
{
    int bytesToRead;
    int thisChunkIndex = 0;	/* 0, 1, ... numChunks-1 */
    int thisChunkStart = 0;	/* 0, chunkSize, ... (numChunks-1)*chunkSize */
    int binstart;
    int freeDiskSpace;
    BOOLEAN notDone;
    int keyPress;
    int quietSize = 0;		/* number of samples bird has been quiet */
    int startWrite = MININT;	/* sample number relative to main buffer */
    int endWrite = MININT;	/* sample number relative to main buffer */
    BOOLEAN newSong = TRUE;
    BOOLEAN inASong = FALSE;
    BOOLEAN meetsTrigCriteria = FALSE;
    BOOLEAN meetsQuietCriteria = TRUE;
    BOOLEAN clipped = FALSE;		/* clipping in a bin? */
    BOOLEAN clippedInChunk = FALSE;	/* clipping in a chunk? */
    float binVariance;
    float minVariance = MAX_VAR;
    float maxVariance = 0.0;
    float sumVariance = 0.0;
    float aveVariance = 0.0;
    int numVariance = 0;
    int binPeak = 0;
    int chunkPeak = 0;

    /* Stereo stuff */
    int j;
    float binVarianceL;
    float binVarianceR;
    float meanVarianceLR;
    float minVarianceL = MAX_VAR;
    float minVarianceR = MAX_VAR;
    float maxVarianceL = 0.0;
    float maxVarianceR = 0.0;
    float sumVarianceL = 0.0;
    float sumVarianceR = 0.0;
    float aveVarianceL = 0.0;
    float aveVarianceR = 0.0;
    int binPeakL = 0;
    int chunkPeakL = 0;
    int binPeakR = 0;
    int chunkPeakR = 0;
    BOOLEAN LTrig = FALSE;
    BOOLEAN RTrig = FALSE;
    BOOLEAN clippedL = FALSE;		/* clipping in a bin? */
    BOOLEAN clippedLInChunk = FALSE;	/* clipping in a chunk? */
    BOOLEAN clippedR = FALSE;		/* clipping in a bin? */
    BOOLEAN clippedRInChunk = FALSE;	/* clipping in a chunk? */
  

#if defined (USE_CURSES)
    mvprintw (STATUS_ROW, STATUS_COL, " Begin ----");
    if ( mono_mode ) {
      mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThresh);
    } else {
      mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
      mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);
    }

#endif
    bytesToRead = chunkSize * bytesPerSamp;

    /* Main loop */
    do {

        thisChunkStart = thisChunkIndex*chunkSize;
	/* ZERO_DBG((logFile, "tcs=%d tci=%d\n", thisChunkStart, thisChunkIndex)); */

        getChunk (audioDevice, &(mainBuffer[thisChunkStart]), bytesToRead);
        chunkPeak = 0;

        for (binstart=0 ; binstart < chunkSize ; binstart+=binSize) {

	  for (j=0; j < binMetaSize; j++) {
	    bufferL[j] = mainBuffer[thisChunkStart+binstart + 2*j];
	    bufferR[j] = mainBuffer[thisChunkStart+binstart + 2*j + 1];
	  }

	  if ( mono_mode ) {
            binVariance = calcVariance(bufferL, binMetaSize, &clipped, &binPeak);
            if (binPeak > chunkPeak) {
	      chunkPeak = binPeak;
	    }
            if (binVariance < minVariance) {
	      minVariance = binVariance;
	    }
            if (binVariance > maxVariance) {
	      maxVariance = binVariance;
	    }
	    sumVariance += binVariance;
	    numVariance++;
            VARIANCE_DBG((logFile, "variance = %f\n", binVariance));
            if (clipped) {
	      clippedInChunk = TRUE;
            }
            meetsTrigCriteria = (binVariance > songThresh);
	    meetsQuietCriteria = (!meetsTrigCriteria);

	  } else {
	    binVarianceL = calcVariance(bufferL, binMetaSize, &clippedL, &binPeakL);
	    binVarianceR = calcVariance(bufferR, binMetaSize, &clippedR, &binPeakR);
            if (binPeakL > chunkPeakL) {
	      chunkPeakL = binPeakL;
	    }
            if (binPeakR > chunkPeakR) {
	      chunkPeakR = binPeakR;
	    }
            if (binVarianceL < minVarianceL) {
	      minVarianceL = binVarianceL;
	    }
            if (binVarianceL > maxVarianceL) {
	      maxVarianceL = binVarianceL;
	    }
            if (binVarianceR < minVarianceR) {
	      minVarianceR = binVarianceR;
	    }
            if (binVarianceR > maxVarianceR) {
	      maxVarianceR = binVarianceR;
	    }
	    meanVarianceLR = (binVarianceL + binVarianceR)/2.0;
	    sumVarianceL += binVarianceL;
	    sumVarianceR += binVarianceR;
	    numVariance++;
            VARIANCE_DBG((logFile, "varianceL = %f, varianceR = %f\n", binVarianceL, binVarianceR));
            if (clippedL) {
	      clippedLInChunk = TRUE;
	    }
            if (clippedR) {
	      clippedRInChunk = TRUE;
	    }
	    if (clippedL || clippedR ) {
	      clippedInChunk = TRUE;
	    }
	    LTrig = (binVarianceL > songThreshL);
	    RTrig = (binVarianceR > songThreshR);
	    switch (trigCriteria) {
	    case LEVEL_CRIT:
	      meetsTrigCriteria = (meanVarianceLR > songThresh);
	      break;
	    case L_OR_R: /* Default */
	      meetsTrigCriteria = (LTrig || RTrig);
	      break;
	    case L_AND_R:
	      meetsTrigCriteria = (LTrig && RTrig);
	      break;
	    case L:
	      meetsTrigCriteria = LTrig;
	      break;
	    case R:
	      meetsTrigCriteria = RTrig;
	      break;
	    case L_NOT_R:
	      meetsTrigCriteria = (LTrig && !RTrig);
	      break;
	    case R_NOT_L:
	      meetsTrigCriteria = (RTrig && !LTrig);
	      break;
	    case L_NOT_R_OR_R_NOT_L:
	      meetsTrigCriteria = ((LTrig && !RTrig) || (RTrig && !LTrig));
	      break;
	    default:
	      die("Impossible stereo trigger criterion in acquire.c!");
	    }

	    switch (quietCriteria) {
	    case QLEVEL_CRIT:
	      meetsQuietCriteria = (meanVarianceLR <= songThresh);
	      break;
	    case QL_AND_R: /* Default */
	      meetsQuietCriteria = (!RTrig && !LTrig);
	      break;
	    case QL:
	      meetsQuietCriteria = (!LTrig);
	      break;
	    case QR:
	      meetsQuietCriteria = (!RTrig);
	      break;
	    case QL_OR_R:
	      meetsQuietCriteria = (!LTrig || !RTrig);
	      break;
	    default:
	      die("Impossible stereo quiet criterion in acquire.c!");
	    }

	  }

	  if (inASong) {
	    if (!meetsQuietCriteria) {
	      /*
	       * bird was singing and still is
	       */
	      STATE_DBG((logFile, "was singing, still is\n"));
	      quietSize = 0;
	      endWrite = thisChunkStart + binstart + binSize;
	      /* ZERO_DBG((logFile, "sw=%d ew=%d tcs=%d bs=%d\n", startWrite, endWrite, thisChunkStart, binstart)); */
	      VARIABLE_DBG((logFile, "QS=%d sw=%d EW=%d\n", quietSize, startWrite, endWrite));
	    } else {
	      /*
	       * bird was singing but is not in this bin
	       */
	      STATE_DBG((logFile, "was singing, now quiet\n"));
	      quietSize += binSize;
	      VARIABLE_DBG((logFile, "QS=%d sw=%d ew=%d\n", quietSize, startWrite, endWrite));
	      if (quietSize > gapSize) {
		/*
		 * bird has been quiet long enough, call it quits
		 */
		STATE_DBG((logFile, "end of song\n"));
		WRITE_DBG((logFile, "end of song\n"));
		endWrite += postTriggerSize;
		VARIABLE_DBG((logFile, "qs=%d sw=%d EW=%d\n", quietSize, startWrite, endWrite));
		ZERO_DBG((logFile, "DON "));
		writeData (startWrite, endWrite, newSong, filenamePrefix);
		inASong = FALSE;
		newSong = TRUE;
		startWrite = MININT;
		endWrite = MININT;
		quietSize = 0;
		VARIABLE_DBG((logFile, "qs=%d SW=%d EW=%d\n", quietSize, startWrite, endWrite));
#if defined (USE_CURSES)
		if (!mono_mode) {
		  /* Redraw channel colors awaiting next trigger */
		  attron(A_UNDERLINE);
		  mvprintw (4, LEFT_CHAN_COL-4,  "Left");
		  mvprintw (4, RIGHT_CHAN_COL-5,  "Right");
		  attroff(A_UNDERLINE);
		}
		mvprintw (STATUS_ROW, STATUS_COL, " Quiet ----");
		refresh();
#endif
	      }
	    }
	  } else {
	    if (meetsTrigCriteria) {
	      /*
	       * bird was not singing but has just started
	       */
	      STATE_DBG((logFile, "wasn't singing, just started\n"));
	      newSong = TRUE;
	      inASong = TRUE;
	      quietSize = 0;
	      startWrite = thisChunkStart + binstart - preTriggerSize;
	      endWrite = thisChunkStart + binstart + binSize;
	      VARIABLE_DBG((logFile, "QS=%d SW=%d EW=%d\n", quietSize, startWrite, endWrite));
	      /* write info below to log? */
	      if (!mono_mode) {
		if (LTrig && !RTrig) {
		  fprintf(logFile, "Left channel triggered acquisition.\n");
		} else if (RTrig && !LTrig) {
		  fprintf(logFile, "Right channel triggered acquisition.\n");
		} else {
		  fprintf(logFile, "Both Left and Right channels triggered acquisition.\n");
		}
	      }
#if defined (USE_CURSES)
	      if (has_colors()) {
		attron(COLOR_PAIR(COLOR_GREEN));
		mvprintw (STATUS_ROW, STATUS_COL, " Singing --");
		if (!mono_mode) {
		  /* Color triggered channels green */
		  attron(A_UNDERLINE);
		  if (LTrig)
		    mvprintw (4, LEFT_CHAN_COL-4,  "Left");
		  if (RTrig)
		    mvprintw (4, RIGHT_CHAN_COL-5,  "Right");
		  attroff(A_UNDERLINE);
		}
		attroff(COLOR_PAIR(COLOR_GREEN));
	      } else {
		mvprintw (STATUS_ROW, STATUS_COL, " Singing --");
	      }
	      refresh();
#endif
	    } else {
	      /*
	       * bird was not singing and still is not
	       */
	      /* nothing to do! */
	      /* note that quietSize only is of concern when inASong==TRUE */
	      STATE_DBG((logFile, "wasn't singing, still isn't\n"));
	    }
	  }
        } /* End loop over chunk analysis bins */
	
        /*
         * we've examined the contents of this chunk.  now do some
         * sanity checks.
         */
	if ((startWrite != MININT) && (endWrite < 0 || endWrite > totalSize)) {
	  fprintf(logFile, "IMPOSSIBLE ERROR: bad endWrite value %d\n", endWrite);
	}
	if ((startWrite != MININT) && (startWrite < -preTriggerSize || startWrite > totalSize)) {
	  fprintf(logFile, "IMPOSSIBLE ERROR: bad startWrite value %d\n", startWrite);
	}
	
	ZERO_DBG((logFile, "tcs=%06d sw=%06d ew=%06d\n", thisChunkStart, startWrite, endWrite));
	
        /*
         * update the index now, so that if bird was singing in final
         * bin then we can flush everything to disk and we will know
         * where the next bit of the song should start.
         */
        thisChunkIndex = (thisChunkIndex + 1) % numChunks;
	
        /*
         * now let's try to schlep some of this stuff off to disk
         */
        if (inASong) {
            if (quietSize == 0) {
                /*
                 * we are in a song and the bird was singing in the final bin
                 * write out all we have, and get ready for next chunk
                 */
                WRITE_DBG((logFile, "singing at end of chunk, write it out\n"));
		ZERO_DBG((logFile, "END "));
                writeData (startWrite, endWrite, newSong, filenamePrefix);
                newSong = FALSE;
                startWrite = thisChunkIndex * chunkSize;
                VARIABLE_DBG((logFile, "qs=%d SW=%d ew=%d\n", quietSize, startWrite, endWrite));
            } else if (startWrite > endWrite) {
                /*
                 * song MIGHT have ended by end of this chunk
                 * however, we have wrapped around
                 * flush some out
                 */
                WRITE_DBG((logFile, "not singing at end of chunk, flush wraparound\n"));
		ZERO_DBG((logFile, "WRP "));
                writeData (startWrite, totalSize, newSong, filenamePrefix);
                newSong = FALSE;
                startWrite = 0;
                VARIABLE_DBG((logFile, "qs=%d SW=%d ew=%d\n", quietSize, startWrite, endWrite));
            } else if ((endWrite - startWrite) > (2 * chunkSize)) {
                /*
                 * song MIGHT have ended by end of this chunk
                 * however, we have some data piling up
                 * flush some out
                 */
                WRITE_DBG((logFile, "not singing at end of chunk, flush some\n"));
		ZERO_DBG((logFile, "2CK "));
                writeData (startWrite, startWrite+chunkSize, newSong, filenamePrefix);
                newSong = FALSE;
                startWrite += chunkSize;
                VARIABLE_DBG((logFile, "qs=%d SW=%d ew=%d\n", quietSize, startWrite, endWrite));
            }
        }


        freeDiskSpace = diskSpace (".");

#if defined (USE_CURSES)
{
static int i = 0;
i++;
if(i==5){
i=0;
	if (mono_mode) {
	  aveVariance = sumVariance / numVariance;
	  mvprintw ( 5, LEFT_CHAN_COL-10, "%10.0f", minVariance);
	  mvprintw ( 6, LEFT_CHAN_COL-10, "%10.0f", aveVariance);
	  mvprintw ( 7, LEFT_CHAN_COL-10, "%10.0f", maxVariance);
	  mvprintw (11, LEFT_CHAN_COL-10, "%10.0f%%", ((float)chunkPeak / 32768.0) * 100.0);
	  minVariance = MAX_VAR;
	  maxVariance = 0.0;
	  sumVariance = 0.0;
	  numVariance = 0;
	} else {
	  aveVarianceL = sumVarianceL / numVariance;
	  aveVarianceR = sumVarianceR / numVariance;
	  mvprintw ( 5, LEFT_CHAN_COL-10, "%10.0f", minVarianceL);
	  mvprintw ( 6, LEFT_CHAN_COL-10, "%10.0f", aveVarianceL);
	  mvprintw ( 7, LEFT_CHAN_COL-10, "%10.0f", maxVarianceL);
	  mvprintw (11, LEFT_CHAN_COL-10, "%10.0f%%", ((float)chunkPeakL / 32768.0) * 100.0);
	  mvprintw ( 5, RIGHT_CHAN_COL-10, "%10.0f", minVarianceR);
	  mvprintw ( 6, RIGHT_CHAN_COL-10, "%10.0f", aveVarianceR);
	  mvprintw ( 7, RIGHT_CHAN_COL-10, "%10.0f", maxVarianceR);
	  mvprintw (11, RIGHT_CHAN_COL-10, "%10.0f%%", ((float)chunkPeakR / 32768.0) * 100.0);

	  minVarianceL = MAX_VAR;
	  maxVarianceL = 0.0;
	  sumVarianceL = 0.0;
	  minVarianceR = MAX_VAR;
	  maxVarianceR = 0.0;
	  sumVarianceR = 0.0;
	  numVariance = 0;
	}

	mvprintw (19, 14, "%d KB ", freeDiskSpace);

        keyPress = getch();
        notDone = TRUE;
	switch (keyPress) {
	    case '*':
		threshScale *= 10;
		if (threshScale > 1000000) {
		    threshScale = 1000000;
		}
		mvprintw (16, 61, "+%0.0f", threshScale);
		mvprintw (17, 60, "+%0.0f", threshScale * 10);
		mvprintw (18, 61, "-%0.0f", threshScale);
		mvprintw (19, 60, "-%0.0f", threshScale * 10);
		break;
	    case '/':
		threshScale /= 10;
		if (threshScale < 1) {
		    threshScale = 1;
		}
		mvprintw (16, 61, "+%0.0f ", threshScale);
		mvprintw (17, 60, "+%0.0f ", threshScale * 10);
		mvprintw (18, 61, "-%0.0f ", threshScale);
		mvprintw (19, 60, "-%0.0f ", threshScale * 10);
		break;
	    case 'l':
		if ( !mono_mode ) {
		  /* In LEVEL_CRIT criterion, channels are always locked */
		  if ( trigCriteria != LEVEL_CRIT ) {
		    channels_locked  = (!channels_locked);
		    /* We equalize to the left channel when locked */
		    if (channels_locked) {
		      songThreshR = songThreshL;
		      mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
		      mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);
		      mvprintw(4, chanlock_col, "<-->");
		    } else if (left_selected) {
		      mvprintw(4, chanlock_col, "<-  ");
		    } else {
		      mvprintw(4, chanlock_col, "  ->");
		    }		    
		  }
		}
		break;
	    case 'c':
		if ( !mono_mode ) {
		  /* In locked state channels can't be selected */
		  if (!channels_locked) {
		    left_selected = (!left_selected);
		    if (left_selected) {
		      mvprintw(4, chanlock_col, "<-  ");
		    } else {
		      mvprintw(4, chanlock_col, "  ->");
		    }
		  }		    
		}
		break;
	    case 'p':
		if ( !mono_mode ) {
		  if ( channels_locked ) {
		    if (trigCriteria == LEVEL_CRIT ) {
		      songThresh += threshScale;
		      songThreshL = songThresh;
		      songThreshR = songThresh;
		    } else {
		      songThreshL += threshScale;
		      songThreshR = songThreshL;
		    }
		    fprintf(logFile, "Threshold (L and R) changed to %.3f\n", songThreshL);
		  } else {
		    if ( left_selected ) {
		      songThreshL += threshScale;
		      fprintf(logFile, "Threshold (L) changed to %.3f\n", songThreshL);
		    } else { 
		      songThreshR += threshScale;
		      fprintf(logFile, "Threshold (R) changed to %.3f\n", songThreshR);
		    }
		  }
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
		  mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);

		} else {
		  songThresh += threshScale;
		  fprintf(logFile, "Threshold changed to %.3f\n", songThresh);
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThresh);
		}

		break;
	    case 'P':
		if ( !mono_mode ) {
		  if ( channels_locked ) {
		    if (trigCriteria == LEVEL_CRIT ) {
		      songThresh += (threshScale * 10);
		      songThreshL = songThresh;
		      songThreshR = songThresh;
		    } else {
		      songThreshL += (threshScale * 10);
		      songThreshR = songThreshL;
		    }
		    fprintf(logFile, "Threshold (L and R) changed to %.3f\n", songThreshL);
		  } else {
		    if ( left_selected ) {
		      songThreshL += (threshScale * 10);
		      fprintf(logFile, "Threshold (L) changed to %.3f\n", songThreshL);
		    } else { 
		      songThreshR += (threshScale * 10);
		      fprintf(logFile, "Threshold (R) changed to %.3f\n", songThreshR);
		    }
		  }
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
		  mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);

		} else {
		  songThresh += (threshScale * 10);
		  fprintf(logFile, "Threshold changed to %.3f\n", songThresh);
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThresh);
		}

		break;
	    case 'm':
		if ( !mono_mode ) {
		  if ( channels_locked ) {
		    if (trigCriteria == LEVEL_CRIT ) {
		      songThresh -= threshScale;
		      if (songThresh < 0) {
			songThresh = 0;
		      }
		      songThreshL = songThresh;
		      songThreshR = songThresh;
		    } else {
		      songThreshL -= threshScale;
		      if (songThreshL < 0) {
			songThreshL = 0;
		      }
		      songThreshR = songThreshL;
		    }
		    fprintf(logFile, "Threshold (L and R) changed to %.3f\n", songThreshL);
		  } else {
		    if ( left_selected ) {
		      songThreshL -= threshScale;
		      if (songThreshL < 0) {
			songThreshL = 0;
		      }
		      fprintf(logFile, "Threshold (L) changed to %.3f\n", songThreshL);
		    } else { 
		      songThreshR -= threshScale;
		      if (songThreshR < 0) {
			songThreshR = 0;
		      }
		      fprintf(logFile, "Threshold (R) changed to %.3f\n", songThreshR);
		    }
		  }
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
		  mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);

		} else {
		  songThresh -= threshScale;
		  if (songThresh < 0) {
		    songThresh = 0;
		  }
		  fprintf(logFile, "Threshold changed to %.3f\n", songThresh);
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThresh);
		}

		break;
	    case 'M':
		if ( !mono_mode ) {
		  if ( channels_locked ) {
		    if (trigCriteria == LEVEL_CRIT ) {
		      songThresh -= (threshScale * 10);
		      if (songThresh < 0) {
			songThresh = 0;
		      }
		      songThreshL = songThresh;
		      songThreshR = songThresh;
		    } else {
		      songThreshL -= (threshScale * 10);
		      if (songThreshL < 0) {
			songThreshL = 0;
		      }
		      songThreshR = songThreshL;
		    }
		    fprintf(logFile, "Threshold (L and R) changed to %.3f\n", songThreshL);
		  } else {
		    if ( left_selected ) {
		      songThreshL -= (threshScale * 10);
		      if (songThreshL < 0) {
			songThreshL = 0;
		      }
		      fprintf(logFile, "Threshold (L) changed to %.3f\n", songThreshL);
		    } else { 
		      songThreshR -= (threshScale * 10);
		      if (songThreshR < 0) {
			songThreshR = 0;
		      }
		      fprintf(logFile, "Threshold (R) changed to %.3f\n", songThreshR);
		    }
		  }
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThreshL);
		  mvprintw (9, RIGHT_CHAN_COL-10, "%10.0f", songThreshR);

		} else {
		  songThresh -= (threshScale * 10);
		  if (songThresh < 0) {
		    songThresh = 0;
		  }
		  fprintf(logFile, "Threshold changed to %.3f\n", songThresh);
		  mvprintw (9, LEFT_CHAN_COL-10, "%10.0f", songThresh);
		}

		break;
	    case 'q':
	    case 'Q':
		notDone = FALSE;
		break;
	    }
	/* See if this fixes screen garbage. */
	refresh();

	if (mono_mode) {
	  if (clippedInChunk) {
	    if ( has_colors() ) {
	      attron(COLOR_PAIR(COLOR_RED));
	    }
	    mvprintw (CLIP_ROW, CLIP_COL, " Clipped! ");
	    if ( has_colors() ) {
	      attroff(COLOR_PAIR(COLOR_RED));
	    }
	    fprintf (logFile, "Data Clipped!!\n");
	    clippedInChunk = FALSE;
	  } else {
	    mvprintw (CLIP_ROW, CLIP_COL, "          ");
	  }
	} else {
	  if (clippedLInChunk) {
	    if ( has_colors() ) {
	      attron(COLOR_PAIR(COLOR_RED));
	    }
	    mvprintw (CLIP_ROW, LEFT_CHAN_COL-10, " Clipped! ");
	    if ( has_colors() ) {
	      attroff(COLOR_PAIR(COLOR_RED));
	    }
	    fprintf (logFile, "Data (L) Clipped!!\n");
	    clippedLInChunk = FALSE;
	  } else {
	    mvprintw (CLIP_ROW, LEFT_CHAN_COL-10, "          ");
	  }
	  if (clippedRInChunk) {
	    if ( has_colors() ) {
	      attron(COLOR_PAIR(COLOR_RED));
	    }
	    mvprintw (CLIP_ROW, RIGHT_CHAN_COL-10, " Clipped! ");
	    if ( has_colors() ) {
	      attroff(COLOR_PAIR(COLOR_RED));
	    }
	    fprintf (logFile, "Data (R) Clipped!!\n");
	    clippedRInChunk = FALSE;
	  } else {
	    mvprintw (CLIP_ROW, RIGHT_CHAN_COL-10, "          ");
	  }
	  clippedInChunk = FALSE;
	}
}else{
notDone = TRUE;
}
}
#else
        notDone = TRUE;
	if (mono_mode) {
	  if (clippedInChunk) {
	    fprintf (logFile, "Data Clipped!!\n");
	    clippedInChunk = FALSE;
	  } 
	} else {
	  if (clippedLInChunk) {
	    fprintf (logFile, "Data (L) Clipped!!\n");
	    clippedLInChunk = FALSE;
	  }
	  if (clippedRInChunk) {
	    fprintf (logFile, "Data (R) Clipped!!\n");
	    clippedRInChunk = FALSE;
	  }
	  clippedInChunk = FALSE;
        }
#endif
    } while ((freeDiskSpace > MINDISKSPACE) && notDone);

    flushFiles();
    return;
}
