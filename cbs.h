/*
 * $Id: cbs.h,v 1.20 2003/06/06 23:59:38 ds Exp $
 */

#include <stdio.h>
#include "wave.h"

/*****
 ***** configuration definitions
 *****/

extern char *audio_device;
extern char *mixer_device;
#define MINDISKSPACE 512        /* stop if free space goes below this KB */
#define ARGLIST "abcdghmnpqstu"

 

/*****
 ***** symbolic definitions
 *****/

#define ROOT_UID     0

#define AUDIO_CHANNELS	2

#define MAX_FILE_NAME_LENGTH 512

#define THRESHSCALE 1000

#define WAV_MONO 1		/* for use in header of RIFF/WAVE files */
#define WAV_STEREO 2
#define SAMP_FREQ_MAX 44100 

#define SND_CARD_MONO 0		/* for use in configuring sound card */
#define SND_CARD_STEREO 1
#define MIXER_ADDR                0x4
#define MIXER_DATA                0x5
#define AGCREG 0x43

/* max and min values for signed shorts so that we can detect clipping */
#define MAX_SHORT 32767
#define MIN_SHORT -32768

#define MAX_VAR 1073741824.0

/* coords for status info under curses */
#define TITLE_ROW  2
#define TITLE_COL  20
#define CLIP_ROW  8
#define CLIP_COL  40
#define FILENAME_ROW 23
#define FILENAME_COL  2
#define STATUS_ROW 23
#define STATUS_COL 50
#define LEFT_CHAN_COL 32
#define RIGHT_CHAN_COL 52

/* #define LEVEL_CRIT 0
   #define FFT_CRIT   1
*/


/*****
 ***** macro definitions
 *****/

/* macros to handle small chores */
#define MAX(a, b) ((a)>(b)?(a):(b))
#define MIN(a, b) ((a)>(b)?(b):(a))
#define SQR(a) ((a)*(a)) 

#define LAST_CHUNK numChunks - 1
#define NEXT_CHUNK ((thisChunk + 1) % numChunks)
#define PREV_CHUNK ((thisChunk > 0) ? (thisChunk - 1) : (numChunks - 1))

/* returns offset of bin in chunk in the main buffer in samples */
#define BUFFEROFFSET(chunk,bin) ((chunk * chunkSize) + (bin * binSize))

#ifdef DEFINE_GLOBALS
#define GLOBAL
#define INIT(x) = x
#else
#define GLOBAL extern
#define INIT(x)
#endif

#ifdef VARIABLE_DEBUG
#define VARIABLE_DBG(blah) fprintf blah
#else
#define VARIABLE_DBG(blah)
#endif

#ifdef ZERO_DEBUG
#define ZERO_DBG(blah) fprintf blah
#else
#define ZERO_DBG(blah)
#endif

#ifdef STATE_DEBUG
#define STATE_DBG(blah) fprintf blah
#else
#define STATE_DBG(blah)
#endif

#ifdef WRITE_DEBUG
#define WRITE_DBG(blah) fprintf blah
#else
#define WRITE_DBG(blah)
#endif

#ifdef VARIANCE_DEBUG
#define VARIANCE_DBG(blah) fprintf blah
#else
#define VARIANCE_DBG(blah)
#endif

/*****
 ***** structure definitions
 *****/

typedef struct {
    int bin;
    int transition;
} trans;

typedef int BOOLEAN;
#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif

/*****
 ***** global variables
 *****/

#if defined (DEBUG)
GLOBAL BOOLEAN debugflag INIT(TRUE);
#else
GLOBAL BOOLEAN debugflag INIT(FALSE);
#endif

GLOBAL WaveHeader WHead;
GLOBAL char animalNumber[80] INIT("test");
GLOBAL FILE *logFile INIT(NULL);

enum TriggerCriteria { LEVEL_CRIT = 0, L_OR_R, L_AND_R, L, R, L_NOT_R, R_NOT_L, L_NOT_R_OR_R_NOT_L };

enum QuietCriteria { QLEVEL_CRIT = 0, QL_AND_R, QL, QR, QL_OR_R };

GLOBAL BOOLEAN mono_mode INIT(1); /* Are we in MONO mode? */
GLOBAL BOOLEAN channels_locked INIT(1); /* Are stereo channels locked? */
GLOBAL int chanlock_col INIT(0);        /* Position for indicating channel selection */
GLOBAL BOOLEAN left_selected INIT(1);   /* Is the left channel selected? */

GLOBAL int bytesPerSamp INIT(2); /* true # of bytes per sample */

/* all Times are in seconds except where noted
 * all Sizes are in true samples, 1 sample per channel (should be one 16-bit word; 2 bytes)
 * all MetaSizes in meta-samples (not multiplied by nchannels).
 */

GLOBAL float binMSTime INIT(2.0);       /* requested duration for analysis in MILLISECONDS */
GLOBAL int binSize INIT(0);             /* calculated size for analysis */
GLOBAL int binMetaSize INIT(0);         /* calculated per channel meta-size for analysis */

GLOBAL float preTriggerTime INIT(0.2);  /* requested preTrigger time to save */
GLOBAL int preTriggerSize INIT(0);      /* calculated preTrigger samples to save */
GLOBAL float postTriggerTime INIT(0.2); /* requested postTrigger time to save */
GLOBAL int postTriggerSize INIT(0);     /* calculated postTrigger samples to save */
GLOBAL float minDurationTime INIT(1.0); /* requested minDuration time to save */
GLOBAL int minDurationSize INIT(0);     /* calculated minDuration samples to save */
GLOBAL float gapTime INIT(1.0);         /* requested gap */
GLOBAL int gapSize INIT(0);             /* calculated gap samples */

GLOBAL int chunkSize INIT(0);           /* size of read from sound card */
GLOBAL int numChunks INIT(0);           /* calculated from lots-o-stuff */
GLOBAL int totalSize INIT(0);           /* calculated total number of samples in buffer */

GLOBAL int trigCriteria INIT(LEVEL_CRIT);   /* how to determine if singing */
GLOBAL int quietCriteria INIT(QLEVEL_CRIT);   /* how to determine if quiet */
GLOBAL float songThresh INIT(10.0);     /* variance threshold for LEVEL_CRIT */
GLOBAL float songThreshL INIT(10.0);     /* Left channel variance threshold for L criterion */
GLOBAL float songThreshR INIT(10.0);     /* Right channel variance threshold for R criterion */
GLOBAL float threshScale INIT(THRESHSCALE); /* scale for modifying threshold */

GLOBAL short *mainBuffer;               /* main buffer */
GLOBAL short *bufferL;                  /* analysis buffer for L channel */
GLOBAL short *bufferR;                  /* analysis buffer for L channel */


/*****
 ***** function prototypes
 *****/

/* cbs.c */

int main (int argc, char *argv[], char *envp[]);
void usage (void);
int parseCommandLine(int ac, char *av[]);
void preMainLoopLogs (void);


/* init.c */

void initializeValues (void);
void initializeScreen (void);
void initFiles (FILE **lfp, char **fnpp, const char *animalNumber);
int initHardware (void);
int initNumChunks (void);
int initChunks (int num, int size);


/* acquire.c */

void getChunk (int audioDevice, short *buffer, int n);
void getSong(int audioDevice, char *filenamePrefix);


/* die.c */

void die(char *fmt, ...);


/* disk.c */

int diskSpace (char *theDir);
void writeData (int start, int end, BOOLEAN newSong, const char *filenamePref);
void startNextFile (const char *filenamePrefix);
void flushFiles(void);


/* level.c */

float calcVariance(short *data, int num, BOOLEAN *ifClip, int *absPeak);

