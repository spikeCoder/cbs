/*
 * $Id: disk.c,v 1.20 2003/04/04 01:59:01 ds Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mntent.h>
#include <unistd.h>
#include <values.h>
#include <sys/vfs.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#if defined (USE_CURSES)
#include <ncurses.h>
#endif
#include "cbs.h"

#define KBYTE 1024

/*
 * "global" variables visible only to this module 
 */
static char outputFileName[MAX_FILE_NAME_LENGTH];
static FILE *outputFile = NULL;
static int dataWrittenSize = 0;
static int outputFileIndex = 0;


int
diskSpace (char *theDir)
{
    static struct statfs buf;
    long size;

    if (EOF == statfs (theDir, &buf)) {
        perror ("statfs() failed in diskSpace");
        return (-1);
    }

/*  printf("Size of blocks on file system is %ld bytes.\n", buf.f_bsize);
    printf("Available blocks on file system is %ld.\n", buf.f_bavail);
    printf("Available space on file system is %ld KB.\n", buf.f_bavail*buf.f_bsize/1024);
*/

    size = buf.f_bavail*(buf.f_bsize/KBYTE); 
    return ((int)size);

}

static size_t fwrite_samples(const short *ptr, int offset, size_t size,
	size_t nmemb, FILE *stream)
{
	size_t ret;

	if(mono_mode){
		short *tmpbuf;
		int i;

		tmpbuf = malloc(2*nmemb);
		for(i=0;i<nmemb/2;i++){
			tmpbuf[i] = ptr[offset + i*2];
		}
		ret = 2 * fwrite(tmpbuf, 2, nmemb/2, stream);
		free(tmpbuf);
	}else{
		ret = fwrite(ptr+offset, size, nmemb, stream);
	}
	return ret;
}

void
writeData (int start, int end, BOOLEAN newSong, const char *filenamePrefix)
/*
 * start and end are in samples relative to main buffer
 */
{
    int samplesToWrite;
    int offset;
    int wrapSamplesToWrite;
    int wrapOffset;
    int retVal;


    if (newSong) {
        startNextFile (filenamePrefix);
    }
    if (outputFile == (FILE *) NULL) {
        fprintf (logFile, "FATAL ERROR. No output file available for writing, this shouldn't happen");
        die ("No output file available for writing, this shouldn't happen");
    }


    samplesToWrite = end - start;
    offset = start;

    if ((samplesToWrite == 0) || (start == MININT) || (end == MININT)) {
        /*
         * hmmm... this really shouldn't happen
         */
        fprintf (logFile, "ERROR. start %6d   end %6d   samples %6d  ???\n",
                     start, end, samplesToWrite);
        return;
    }

    if (end > totalSize) {
        /*
         *       -------------------------
         *       | m a i n   b u f f e r |
         *       -------------------------
         *                          --------------
         *                          |start -> end|
         *                          --------------
         *
         * flushing out song with posttrigger stuff.
         * we got wraparound in circular buffer.
         * we'll need two writes to get it all.
         */
        wrapSamplesToWrite = totalSize - start;
        wrapOffset = start;
        if (debugflag) {
            fprintf (logFile, "WRITEdata: start %6d   end %6d   samples %6d   offset %6d\n",
                     start, end, wrapSamplesToWrite, wrapOffset);
        }
        retVal = fwrite_samples (mainBuffer, wrapOffset, bytesPerSamp, wrapSamplesToWrite, outputFile);
        dataWrittenSize += retVal;

        if (retVal != wrapSamplesToWrite) {
            fprintf (logFile,
                   "ERROR. wrote %d of %d samples (posttrigger wrap).\n", retVal, wrapSamplesToWrite);
	    perror("");
        }
        offset = 0;
        samplesToWrite = end - totalSize;
    }

    if (start < 0 && end >= 0) {
        /*
         *       -------------------------
         *       | m a i n   b u f f e r |
         *       -------------------------
         *  --------------
         *  |start -> end|
         *  --------------
         *
         * picking up pretrigger stuff.
         * we got wraparound in circular buffer.
         * we'll need two writes to get it all.
         */
        wrapSamplesToWrite = -offset;
        wrapOffset = totalSize - wrapSamplesToWrite;
        if (debugflag) {
            fprintf (logFile, "WRItedATA: start %6d   end %6d   samples %6d   offset %6d\n",
                     start, end, wrapSamplesToWrite, wrapOffset);
        }
        retVal = fwrite_samples (mainBuffer, wrapOffset, bytesPerSamp, wrapSamplesToWrite, outputFile);
        dataWrittenSize += retVal;

        if (retVal != wrapSamplesToWrite) {
            fprintf (logFile,
                   "ERROR. wrote %d of %d samples (pretrigger wrap).\n", retVal, wrapSamplesToWrite);
	    perror("");
        }
        offset = 0;
        samplesToWrite = end;
    }

    if (start < 0 && end < 0) {
        /*
         *                  -------------------------
         *                  | m a i n   b u f f e r |
         *                  -------------------------
         *  --------------
         *  |start -> end|
         *  --------------
         *
         * picking up pretrigger stuff.
         * this is actually a disguised case of the "normal" write:
	 *       -------------------------
	 *       | m a i n   b u f f e r |
	 *       -------------------------
	 *               --------------
	 *               |start -> end|
	 *               --------------
	 * therefore, we just need to adjust things to reflect
	 * this fact and let the regular routine handle writing.
         */
        if (debugflag) {
            fprintf (logFile, "writedata: start %6d   end %6d   samples %6d   offset xxxxxx\n",
                     start, end, samplesToWrite);
        }
        offset = totalSize + start;
    }

    if (start > end) {
        /*
         *       -------------------------
         *       | m a i n   b u f f e r |
         *       -------------------------
         *       -------         ---------
         *       -> end|         |start ->
         *       -------         ---------
         *
         * simple wraparound in circular buffer.
         * we'll need two writes to get it all.
         */
        wrapSamplesToWrite = totalSize - start;
        wrapOffset = start;
        if (debugflag) {
            fprintf (logFile, "writeDATA: start %6d   end %6d   samples %6d   offset %6d\n",
                     start, end, wrapSamplesToWrite, wrapOffset);
        }
        retVal = fwrite_samples (mainBuffer, wrapOffset, bytesPerSamp, wrapSamplesToWrite, outputFile);
        dataWrittenSize += retVal;

        if (retVal != wrapSamplesToWrite) {
            fprintf (logFile,
                   "ERROR. wrote %d of %d samples (spanning wrap).\n", retVal, wrapSamplesToWrite);
	    perror("");
        }
        offset = 0;
        samplesToWrite = end;
    }

    /*
     *       -------------------------
     *       | m a i n   b u f f e r |
     *       -------------------------
     *          --------------
     *          |start -> end|
     *          --------------
     *
     * now do the regular write
     * most of the time only this portion will be executed
     */
    if (debugflag) {
        fprintf (logFile, "writeData: start %6d   end %6d   samples %6d   offset %6d\n",
                 start, end, samplesToWrite, offset);
    }
    retVal = fwrite_samples (mainBuffer, offset, bytesPerSamp, samplesToWrite, outputFile);
    dataWrittenSize += retVal;

    if (retVal != samplesToWrite) {
        fprintf (logFile,
                 "ERROR. wrote %d of %d samples.\n", retVal, samplesToWrite);
	perror("");
    }
    return;
}

void
startNextFile (const char *filenamePrefix)
{
    struct timeval timeVal;
    struct timezone timeZone;
    struct stat statbuf;


    if ((outputFile != NULL) && (dataWrittenSize > 0)) {
        /*
         * fix header of previous file to reflect actual contents
         */
	if(mono_mode){
            WHead.data_length = dataWrittenSize * bytesPerSamp / 2;
	}else{
            WHead.data_length = dataWrittenSize * bytesPerSamp;
	}
        WHead.length = WHead.data_length + WAVE_HEADER_SIZE;
        if (dataWrittenSize < (preTriggerSize + minDurationSize + postTriggerSize)) {
	    fclose (outputFile);
	    fprintf (logFile, "File: %s length %d msec too short, DISCARDED\n", outputFileName,
	             (int) ((float) WHead.data_length * 1000.0 / (float) WHead.bytesPerSec));
	    if (unlink(outputFileName) != 0) {
		fprintf (logFile, "ERROR. can't unlink %s\n", outputFileName);
		perror("");
	    }
	    outputFileIndex--;
        } else {
	    rewind (outputFile);
	    /* XXX hack */
	    if(mono_mode){
		WHead.channels = 1;
	    }else{
		WHead.channels = 2;
	    }
	    WHead.blockAlign = 2 * WHead.channels;
	    WHead.bytesPerSec = WHead.blockAlign * WHead.sample_fq;
	    if (fwrite (&WHead, sizeof (WHead), 1, outputFile) != 1) {
		fprintf (logFile,
			 "ERROR. final update to header of %s\n", outputFileName);
		perror("");
	    }
	    WHead.channels = 2;
	    fclose (outputFile);
	    fprintf (logFile, "File: %s length %d msec, DONE\n", outputFileName,
	             (int) ((float) WHead.data_length * 1000.0 / (float) WHead.bytesPerSec));
        }
        WHead.data_length = dataWrittenSize = 0;
        WHead.length = WHead.data_length + WAVE_HEADER_SIZE;
    }
    /*
     * Now open next file, DON'T CLOBBER EXISTING FILES
     */
    sprintf (outputFileName, "%s%04d", filenamePrefix, ++outputFileIndex);
    while (stat(outputFileName, &statbuf) == 0) {
	sprintf (outputFileName, "%s%04d", filenamePrefix, ++outputFileIndex);
    }

    if ((outputFile = fopen (outputFileName, "w")) == NULL) {
        fprintf (logFile, "FATAL ERROR. Can't open output file %s\n", outputFileName);
        die ("Can't open output file %s", outputFileName);
    } else {
        gettimeofday (&timeVal, &timeZone);
        fprintf (logFile, "File: %s opened at %ld.%06ld\n",
                 outputFileName, timeVal.tv_sec, timeVal.tv_usec);
    }

    /*
     * write header 
     */
    if (fwrite (&WHead, sizeof (WHead), 1, outputFile) != 1) {
        fprintf (logFile, "FATAL ERROR. Can't write WAVE header information to %s", outputFileName);
        die ("Can't write WAVE header information to %s", outputFileName);
    }
#if defined (USE_CURSES)
    mvprintw (FILENAME_ROW, FILENAME_COL, " %s ", outputFileName);
#endif

    return;
}

void
flushFiles (void)
{
    /*
     * done with this run of program, cleanup all files 
     */

    if ((outputFile != NULL) && (dataWrittenSize > 0)) {
        /*
         * fix header of previous file to reflect actual contents
         */
	if(mono_mode){
            WHead.data_length = dataWrittenSize * bytesPerSamp / 2;
	}else{
            WHead.data_length = dataWrittenSize * bytesPerSamp;
	}
        WHead.length = WHead.data_length + WAVE_HEADER_SIZE;
        if (dataWrittenSize < (preTriggerSize + minDurationSize + postTriggerSize)) {
            fclose (outputFile);
	    fprintf (logFile, "File: %s length %d msec too short, DISCARDED\n", outputFileName,
	             (int) ((float) WHead.data_length * 1000.0 / (float) WHead.bytesPerSec));
            if (unlink(outputFileName) != 0) {
                fprintf (logFile, "ERROR. can't unlink %s\n", outputFileName);
                perror("");
            }
	    outputFileIndex--;
        } else {
	    rewind (outputFile);
	    /* XXX hack */
	    if(mono_mode){
		WHead.channels = 1;
	    }else{
		WHead.channels = 2;
	    }
	    WHead.blockAlign = 2 * WHead.channels;
	    WHead.bytesPerSec = WHead.blockAlign * WHead.sample_fq;
	    if (fwrite (&WHead, sizeof (WHead), 1, outputFile) != 1) {
		fprintf (logFile,
			 "ERROR. final update to header of %s\n", outputFileName);
		perror("");
	    }
	    WHead.channels = 2;
	    fclose (outputFile);
	    fprintf (logFile, "File: %s length %d msec, DONE\n", outputFileName,
	             (int) ((float) WHead.data_length * 1000.0 / (float) WHead.bytesPerSec));
        }
    }
    fclose (logFile);
}
