%define name cbs
%define version 2
%define release 2mdk

Summary: Catch Bird Singing
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{version}.tar.bz2
Packager: Brian D. Wright <bdwright@phy.ucsf.edu>
License: GPL
Group: Sound
Url: http://condor.ucsf.edu
Vendor: Brian D. Wright <bdwright@phy.ucsf.edu>
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildRequires: libncurses-devel, libtermcap-devel
Requires: perl, perl-doc

%description
Doupe lab application for recording bird song, otherwise known as 
"Catch Bird Singing".

%prep
%setup -q -n %{name}

%build
#CFLAGS="${CFLAGS:-%optflags}" ; export CFLAGS ; \
#CXXFLAGS="${CXXFLAGS:-%optflags}" ; export CXXFLAGS ; \

make CURSES_LIBDIR=%{_prefix}/%{_lib} TERMCAP_LIBDIR=/%{_lib}

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

make DESTDIR=$RPM_BUILD_ROOT prefix=%{_prefix} install

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/cbs
%{_bindir}/cbs_axe
%{_bindir}/deletesongs.pl
%{_bindir}/getcbstime.pl
%{_bindir}/cbs_rm_unviewed
%{_bindir}/cbs_save
%{_bindir}/cbsdumphead

%changelog
* Fri Nov 24 2006 Brian D. Wright <bdwright@phy.ucsf.edu> 1.8-2mdk
- Fix 64-bit bug in cbsdumphead Makefile.

* Fri Nov 24 2006 Brian D. Wright <bdwright@phy.ucsf.edu> 1.8-1mdk
- Initial rpm cbs package.
